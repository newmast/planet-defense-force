﻿namespace Assets.GameLogic.TapEvents
{
    using ActionMenu;
    using Constants;
    using UnityEngine;

    public class OnTapGround : MonoBehaviour, ITapBehaviour
    {
        private ActionsMenuManager actionMenuManager;

        private void Start()
        {
            actionMenuManager = GameObject.FindGameObjectWithTag(Tags.ActionsMenuManager).GetComponent<ActionsMenuManager>();
        }

        public void OnTapped(RaycastHit hitInfo)
        {
            actionMenuManager.ShowBuildMenu(hitInfo, transform.parent);
        }
    }
}
