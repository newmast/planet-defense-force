﻿using Assets.GameLogic.TapEvents;
using Assets.Utils;
using UnityEngine;
using UnityEngine.EventSystems;

public class TapDetector : MonoBehaviour
{
    [SerializeField]
    private GameObject tapParticlePrefab;

    private void HandleTap(TKTapRecognizer recognizer)
    {
        var tappingUIElement = EventSystem.current.IsPointerOverGameObject();

        if (!tappingUIElement)
        {
            var hitInfo = new RaycastHit();
            var clickedObject = GameplayUtils.GetTappedObject(recognizer.touchLocation(), out hitInfo);

            Instantiate(tapParticlePrefab, hitInfo.point, Quaternion.identity);

            if (clickedObject != null)
            {
                var tapBehaviour = clickedObject.GetComponent<ITapBehaviour>();
                if (tapBehaviour != null)
                {
                    tapBehaviour.OnTapped(hitInfo);
                }
            }
        }
    }

    private void Start()
    {
        var tapRecogizer = new TKTapRecognizer();

        tapRecogizer.numberOfTapsRequired = 1;
        tapRecogizer.enabled = true;
        tapRecogizer.gestureRecognizedEvent += HandleTap;

        TouchKit.addGestureRecognizer(tapRecogizer);
    }
}
