﻿namespace Assets.GameLogic.TapEvents
{
    using UnityEngine;

    public interface ITapBehaviour
    {
        void OnTapped(RaycastHit hitInfo);
    }
}
