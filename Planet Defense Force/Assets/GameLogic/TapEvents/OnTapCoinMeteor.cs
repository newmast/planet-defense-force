﻿using UnityEngine;
using Assets.GameLogic.TapEvents;
using Assets.GameLogic.Resources;
using Assets.Constants;

public class OnTapCoinMeteor : MonoBehaviour, ITapBehaviour
{
    private GatheredResources gatheredResources;
    private GameObject coinManager;

    private void Start()
    {
        gatheredResources = GameObject.FindGameObjectWithTag(Tags.GatheredResources).GetComponent<GatheredResources>();
        coinManager = GameObject.FindGameObjectWithTag(Tags.CoinMeteorManager).gameObject;
    }

    public void OnTapped(RaycastHit hitInfo)
    {
        gatheredResources.AddGold(100);

        Destroy(gameObject);
        Destroy(coinManager);
    }
}
