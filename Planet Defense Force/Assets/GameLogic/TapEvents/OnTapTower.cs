﻿namespace Assets.GameLogic.TapEvents
{
    using ActionMenu;
    using Constants;
    using UnityEngine;

    public class OnTapTower : MonoBehaviour, ITapBehaviour
    {
        private ActionsMenuManager actionMenuManager;

        private void Start()
        {
            actionMenuManager = GameObject.FindGameObjectWithTag(Tags.ActionsMenuManager).GetComponent<ActionsMenuManager>();
        }

        public void OnTapped(RaycastHit hitInfo)
        {
            actionMenuManager.ShowActionsMenu(transform.parent.gameObject);
        }
    }
}
