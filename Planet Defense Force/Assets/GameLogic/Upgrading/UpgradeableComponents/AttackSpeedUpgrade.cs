﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Data.Models;
    using Data.Models.Upgrades;
    using Data.Services;
    using Components;
    using Utils;
    using UnityEngine;

    public class AttackSpeedUpgrade : IUpgrade
    {
        private AttackSpeed attackSpeed;
        private ActionItemData actionData;
        private AllDataValues<AttackSpeedModel> model;

        public AttackSpeedUpgrade(TowerUpgradeManager tum, AttackSpeed attackSpeedComponent, string dataPath)
        {
            attackSpeed = attackSpeedComponent;
            actionData = new ActionsService().GetAttackSpeedActionModel(dataPath);
            model = new DataValuesService().GetAttackSpeedData(dataPath);

            tum.Register(this);
        }

        public Component GetComponent()
        {
            return attackSpeed;
        }

        public void ApplyUpgrade(int level)
        {
            attackSpeed.CurrentAttackSpeed = ModelUtils.GetLevelDetails(model, level).value;
        }
    
        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }

    }
}
