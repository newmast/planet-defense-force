﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Data.Models;
    using Data.Models.Upgrades;
    using Data.Services;
    using Components;
    using Utils;
    using UnityEngine;

    public class RangeUpgrade : IUpgrade
    {
        [SerializeField]
        private RangeData rangeData;

        private ActionItemData actionData;
        private AllDataValues<RangeModel> model;

        public RangeUpgrade(TowerUpgradeManager tum, RangeData component, string dataPath)
        {
            rangeData = component;
            actionData = new ActionsService().GetRangeActionModel(dataPath);
            model = new DataValuesService().GetRangeData(dataPath);

            tum.Register(this);
        }

        public Component GetComponent()
        {
            return rangeData;
        }
    
        public void ApplyUpgrade(int level)
        {
            rangeData.RangeComponent.CurrentRange = ModelUtils.GetLevelDetails(model, level).value;
        }
    
        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }

    }
}
