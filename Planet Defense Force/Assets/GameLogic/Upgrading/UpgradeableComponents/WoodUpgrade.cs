﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Data.Models;
    using Data.Models.Upgrades;
    using Data.Services;
    using Components;
    using Utils;
    using UnityEngine;

    public class WoodUpgrade : IUpgrade
    {
        private Wood woodComponent;
        private ActionItemData actionData;
        private AllDataValues<WoodModel> model;

        public WoodUpgrade(TowerUpgradeManager tum, Wood component)
        {
            woodComponent = component;
            actionData = new ActionsService().GetWoodActionModel();
            model = new DataValuesService().GetWoodData();

            tum.Register(this);
        }

        public void ApplyUpgrade(int level)
        {
            woodComponent.WoodPerInterval = ModelUtils.GetLevelDetails(model, level).value;
        }

        public Component GetComponent()
        {
            return woodComponent;
        }

        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }
    }
}
