﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Assets.Data.Models;
    using UnityEngine;
    using Assets.GameLogic.Components;
    using Data.Models.Upgrades;
    using Data.Services;
    using Utils;
    public class BounceCountUpgrade : IUpgrade
    {
        private BounceCount bounceCount;
        private ActionItemData actionData;
        private AllDataValues<BounceCountModel> model;

        public BounceCountUpgrade(TowerUpgradeManager tum, BounceCount bounceCountComponent, string dataPath)
        {
            bounceCount = bounceCountComponent;
            actionData = new ActionsService().GetBounceCountActionModel(dataPath);
            model = new DataValuesService().GetBounceCountData(dataPath);

            tum.Register(this);
        }

        public Component GetComponent()
        {
            return bounceCount;
        }

        public void ApplyUpgrade(int level)
        {
            bounceCount.BouncesLeft = ModelUtils.GetLevelDetails(model, level).allowedBounces;
        }

        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }

    }
}
