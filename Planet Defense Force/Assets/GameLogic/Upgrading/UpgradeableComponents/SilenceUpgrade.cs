﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Assets.Data.Models.Upgrades;
    using Assets.Data.Services;
    using Assets.GameLogic.Components;
    using UnityEngine;
    using Assets.Data.Models;
    using Utils;

    public class SilenceUpgrade : IUpgrade
    {
        private Silence silence;
        private ActionItemData actionData;
        private AllDataValues<SilenceModel> model;

        public SilenceUpgrade(TowerUpgradeManager tum, Silence silenceComponent, string dataPath)
        {
            silence = silenceComponent;
            actionData = new ActionsService().GetSilenceActionModel(dataPath);
            model = new DataValuesService().GetSilenceData(dataPath);

            tum.Register(this);
        }

        public Component GetComponent()
        {
            return silence;
        }

        public void ApplyUpgrade(int level)
        {
            silence.SilenceTime = ModelUtils.GetLevelDetails(model, level).silenceDuration;
        }

        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }

    }
}
