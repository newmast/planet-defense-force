﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Assets.Data.Models;
    using Assets.Data.Models.Upgrades;
    using Assets.GameLogic.Components;
    using Data.Services;
    using UnityEngine;
    using Utils;

    public class ExplosionRadiusUpgrade : IUpgrade
    {
        private ExplosionRadius explosionRadius;
        private ActionItemData actionData;
        private AllDataValues<ExplosionRadiusModel> model;

        public ExplosionRadiusUpgrade(TowerUpgradeManager tum, ExplosionRadius explosionRadiusComponent, string dataPath)
        {
            explosionRadius = explosionRadiusComponent;
            actionData = new ActionsService().GetExplosionRadiusActionModel(dataPath);
            model = new DataValuesService().GetExplosionRadiusData(dataPath);

            tum.Register(this);
        }

        public Component GetComponent()
        {
            return explosionRadius;
        }

        public void ApplyUpgrade(int level)
        {
            explosionRadius.Radius = ModelUtils.GetLevelDetails(model, level).radius;
            explosionRadius.AoeDamage = ModelUtils.GetLevelDetails(model, level).aoeDamage;
        }

        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }

    }
}
