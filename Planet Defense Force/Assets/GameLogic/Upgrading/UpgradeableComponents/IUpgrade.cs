﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Data.Models;
    using UnityEngine;

    public interface IUpgrade
    {
        Component GetComponent();

        ActionItemData GetActionItemData(); 

        int MaxLevel { get; set; }

        void ApplyUpgrade(int level);
    }
}
