﻿namespace Assets.GameLogic.Upgrading.UpgradeableComponents
{
    using Data.Models;
    using Data.Models.Upgrades;
    using Data.Services;
    using Components;
    using Utils;
    using UnityEngine;

    public class DamageUpgrade : IUpgrade
    {
        private Damage damage;
        private ActionItemData actionData;
        private AllDataValues<DamageModel> model; 

        public DamageUpgrade(TowerUpgradeManager tum, Damage damageComponent, string dataPath)
        {
            damage = damageComponent;
            actionData = new ActionsService().GetDamageActionModel(dataPath);
            model = new DataValuesService().GetDamageData(dataPath);

            tum.Register(this);
        }

        public Component GetComponent()
        {
            return damage;
        }
        
        public void ApplyUpgrade(int level)
        {
            damage.CurrentDamage = ModelUtils.GetLevelDetails(model, level).value;
        }
    
        public ActionItemData GetActionItemData()
        {
            return actionData;
        }

        public int MaxLevel { get; set; }
    }
}
