﻿namespace Assets.GameLogic.Upgrading
{
    using UpgradeableComponents;
    using UnityEngine;

    public class TowerUpgradeManager : MonoBehaviour
    {
        [SerializeField]
        private LevelingManager levelingManager;

        public int GetLevel(Component upgradeComponent)
        {
            return levelingManager.GetLevel(upgradeComponent);
        }

        public bool IsMaxLevel(IUpgrade upgrade)
        {
            return levelingManager.GetLevel(upgrade.GetComponent()) >= upgrade.MaxLevel;
        }

        public bool Register(IUpgrade upgrade)
        {
            if (!levelingManager.ComponentExists(upgrade.GetComponent()))
            {
                Upgrade(upgrade);
                return true;
            }

            return false;
        }

        public void Upgrade(IUpgrade upgrade)
        {
            var newLevel = levelingManager.LevelUp(upgrade.GetComponent());
            upgrade.ApplyUpgrade(newLevel);
        }
    }
}