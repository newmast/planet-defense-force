﻿namespace Assets.GameLogic.Upgrading
{
    using System.Collections.Generic;
    using UnityEngine;

    public class LevelingManager : MonoBehaviour
    {
        private Dictionary<Component, UpgradeLevelModel> levels;

        private void Start()
        {
            levels = new Dictionary<Component, UpgradeLevelModel>();
        }

        public void SetDefaultIfNonexistent(Component component)
        {
            if (!ComponentExists(component))
            {
                levels.Add(
                    component, 
                    new UpgradeLevelModel
                    {
                         CurrentLevel = 0,
                         MaxLevel = 10
                    });
            }
        }

        public bool ComponentExists(Component component)
        {
            return levels.ContainsKey(component);
        }

        public int GetLevel(Component component)
        {
            return levels[component].CurrentLevel;
        }

        public int LevelUp(Component component)
        {
            SetDefaultIfNonexistent(component);

            if (levels[component].CurrentLevel + 1 <= levels[component].MaxLevel)
            {
                levels[component].CurrentLevel++;
            }

            return levels[component].CurrentLevel;
        }
    }
}
