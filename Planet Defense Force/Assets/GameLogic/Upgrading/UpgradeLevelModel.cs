﻿namespace Assets.GameLogic.Upgrading
{
    public class UpgradeLevelModel
    {
        public int CurrentLevel { get; set; }

        public int MaxLevel { get; set; }
    }
}
