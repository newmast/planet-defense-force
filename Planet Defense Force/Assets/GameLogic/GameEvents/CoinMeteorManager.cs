﻿namespace Assets.GameLogic.GameEvents
{
    using Common;
    using Constants;
    using UnityEngine;

    public class CoinMeteorManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject coin;

        private GameProgressData gameProgressData;
        private GameObject coinObject;

        private void Start()
        {
            gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();

            if (!gameProgressData.Current.IsUnlocked(SaveIds.RandomCoinMeteorId))
            {
                Destroy(gameObject);
            }

            if (!gameProgressData.Current.IsUnlocked(SaveIds.MoonId))
            {
                Destroy(GameObject.Find("Moon"));
            }

            Invoke("SpawnRandomMeteor", Random.Range(20, 60));
        }

        private void Update()
        {
            if (coinObject != null)
            {
                coinObject.transform.position += 5 * coin.transform.forward * Time.deltaTime;
            }
        }

        public void SpawnRandomMeteor()
        {
            var pos = -Random.onUnitSphere * 15;

            coinObject = Instantiate(coin, pos + new Vector3(2, 2, 2), Quaternion.identity) as GameObject;
        }
    }
}