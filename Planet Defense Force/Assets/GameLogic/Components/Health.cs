﻿namespace Assets.GameLogic.Components
{
    using System;
    using UnityEngine;

    public class Health : MonoBehaviour
    {
        public float CurrentHealth { get; set; }

        public float MaxHealth { get; set; }

        public Action OnDeath { get; set; }

        public void Heal(float addedHealth)
        {
            CurrentHealth += addedHealth;
            if (CurrentHealth > MaxHealth)
            {
                CurrentHealth = MaxHealth;
            }
        }

        public void TakeDamage(float removedHealth)
        {
            if (IsDead())
            {
                return;
            }

            CurrentHealth -= removedHealth;
            if (CurrentHealth <= Mathf.Epsilon)
            {
                CurrentHealth = 0;
            }

            if (IsDead() && OnDeath != null)
            {
                OnDeath.Invoke();
            }
        }

        public bool IsDead()
        {
            return CurrentHealth <= Mathf.Epsilon;
        }
    }
}
