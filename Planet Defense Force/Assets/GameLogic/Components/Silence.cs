﻿namespace Assets.GameLogic.Components
{
    using Shooting;
    using UnityEngine;

    public class Silence : MonoBehaviour
    {
        [SerializeField]
        private Shooter shooter;

        public void Start()
        {
            shooter.OnBulletShot = (bullet) =>
            {
                bullet.GetComponent<EmpBullet>().SilenceTime = SilenceTime;
            };
        }

        public float SilenceTime { get; set; }
    }
}
