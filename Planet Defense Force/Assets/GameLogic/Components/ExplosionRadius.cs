﻿namespace Assets.GameLogic.Components
{
    using Shooting;
    using UnityEngine;

    public class ExplosionRadius : MonoBehaviour
    {
        [SerializeField]
        private Shooter shooter;

        private void Start()
        {
            shooter.OnBulletShot = (bullet) =>
            {
                bullet.GetComponent<ExplodingBullet>().SetExplosionParameters(AoeDamage, Radius);
            };
        }

        public float Radius { get; set; }

        public float AoeDamage { get; set; }
    }
}
