﻿namespace Assets.GameLogic.Components
{
    using UnityEngine;

    public class Damage : MonoBehaviour
    {
        public float CurrentDamage { get; set; }
    }
}
