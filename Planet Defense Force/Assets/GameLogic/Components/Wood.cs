﻿namespace Assets.GameLogic.Components
{
    using Constants;
    using Extensions;
    using Resources;
    using UnityEngine;

    public class Wood : MonoBehaviour
    {
        [SerializeField]
        private int intervalInSeconds;

        private GatheredResources resources;

        private void Start()
        {
            resources = GameObject.FindGameObjectWithTag(Tags.GatheredResources).GetComponent<GatheredResources>();
            this.InvokeRepeating(AddResources, 0, intervalInSeconds);
        }

        public int WoodPerInterval { get; set; }

        public void AddResources()
        {
            resources.AddWood(WoodPerInterval);
        }
    }
}
