﻿namespace Assets.GameLogic.Components
{
    using UnityEngine;

    public class AttackSpeed : MonoBehaviour
    {
        public float CurrentAttackSpeed { get; set; }

        public float GetTimeBetweenAttacks()
        {
            return 1f / CurrentAttackSpeed;
        }
    }
}
