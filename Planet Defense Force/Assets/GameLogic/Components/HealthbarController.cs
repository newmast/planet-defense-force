﻿namespace Assets.GameLogic.Components
{
    using UnityEngine;
    using UnityEngine.UI;

    public class HealthbarController : MonoBehaviour
    {
        [SerializeField]
        private Image currentHealthImage;

        [SerializeField]
        private Health health;

        [SerializeField]
        private Canvas healthbarCanvas;

        private void UpdateHealthbar()
        {
            var percentage = health.CurrentHealth / health.MaxHealth;
            currentHealthImage.fillAmount = percentage;
        }

        private void Update()
        {
            UpdateHealthbar();
        }
    }
}
