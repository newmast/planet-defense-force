﻿namespace Assets.GameLogic.Components
{
    using System;
    using UnityEngine;

    public class Range : MonoBehaviour
    {
        private SphereCollider sphereCollider;

        private void OnTriggerStay(Collider other)
        {
            if (OnTriggerStayAction != null)
            {
                OnTriggerStayAction.Invoke(other);
            }
        }

        public void OnTriggerEnter(Collider other)
        {
            if (OnTriggerEnterAction != null)
            {
                OnTriggerEnterAction.Invoke(other);
            }
        }

        public void OnTriggerExit(Collider other)
        {
            if (OnTriggerExitAction != null)
            {
                OnTriggerExitAction.Invoke(other);
            }
        }

        public Action<Collider> OnTriggerEnterAction { get; set; }

        public Action<Collider> OnTriggerStayAction { get; set; }

        public Action<Collider> OnTriggerExitAction { get; set; }

        public float CurrentRange
        {
            get
            {
                if (sphereCollider == null)
                {
                    sphereCollider = Collider;
                }

                return sphereCollider.radius;
            }
            set
            {
                if (sphereCollider == null)
                {
                    sphereCollider = Collider;
                }

                sphereCollider.radius = value;
            }
        }
    
        public SphereCollider Collider
        {
            get
            {
                if (sphereCollider == null)
                {
                    sphereCollider = GetComponent<SphereCollider>();
                }

                return sphereCollider;
            }
            set
            {
                sphereCollider = value;
            }
        }

        public bool IsInRange(Vector3 other)
        {
            return Vector3.Distance(other, transform.position) <= CurrentRange;
        }
    }
}
