﻿namespace Assets.GameLogic.Components
{
    using UnityEngine;

    public class RangeData : MonoBehaviour
    {
        private Range rangeComponent;
        private SphereCollider sphereColliderComponent;

        private void Awake()
        {
            rangeComponent = GetComponent<Range>();
        }
	
        public Range RangeComponent
        {
            get
            {
                if (rangeComponent == null)
                {
                    rangeComponent = GetComponent<Range>();
                }

                return rangeComponent;
            }
            set
            {
                rangeComponent = value;
            }
        }

        public SphereCollider SphereComponent
        {
            get
            {
                if (rangeComponent == null)
                {
                    rangeComponent = RangeComponent;
                }

                return rangeComponent.Collider;
            }
            set
            {
                rangeComponent.Collider = value;
            }
        }
    }
}
