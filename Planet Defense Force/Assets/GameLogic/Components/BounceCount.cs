﻿namespace Assets.GameLogic.Components
{
    using UnityEngine;

    public class BounceCount : MonoBehaviour
    {
        public float BouncesLeft { get; set; }
    }
}
