﻿using UnityEngine;

namespace Assets.GameLogic.Tutorial
{
    using System;
    using ActionMenu;
    using Constants;
    using Data.Models.Tutorial;
    using Data.Services;
    using EnemyLogic;
    using InfoMessages;
    using LevelMenu;

    public class TutorialManager : MonoBehaviour
    {
        [SerializeField]
        private ActionsMenuManager actionsMenuManager;

        [SerializeField]
        private EnemySpawnSystem enemySpawnSystem;

        private AllTutorials tutorials;

        private void Awake()
        {
            tutorials = new TutorialService().GetAllTutorials();
        }

        private void Start()
        {
            var chosenLevelData = GameObject.Find(FixedGameObjects.ChosenLevelData).GetComponent<ChosenLevelData>();
            if (chosenLevelData.Level != 1)
            {
                Destroy(gameObject);
                return;
            }
            
            ShowTutorial(tutorials.welcome, () => ShowTutorial(tutorials.landTap, () => { }));

            actionsMenuManager.OnActionMenuClosed = () => 
                ShowTutorial(tutorials.tapTower,
                    () => actionsMenuManager.OnActionMenuClosed = null);

            enemySpawnSystem.OnGroupSpawned = () =>
                ShowTutorial(tutorials.incomingAliens,
                    () => enemySpawnSystem.OnGroupSpawned = null);
        }

        private void ShowTutorial(Tutorial tutorial, Action onClosed)
        {
            InfoMessageFactory.Create(tutorial.title, tutorial.content, onClosed);
        }
    }
}
