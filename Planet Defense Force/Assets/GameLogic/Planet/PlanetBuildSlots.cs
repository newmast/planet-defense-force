﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.GameLogic.Planet
{
    public class PlanetBuildSlots
    {
        private Dictionary<int, GameObject> slots;

        public PlanetBuildSlots()
        {
            slots = new Dictionary<int, GameObject>();
        }

        public bool IsFree(int triangleIndex)
        {
            if (!slots.ContainsKey(triangleIndex))
            {
                return true;
            }

            return slots[triangleIndex] == null;
        }

        public void TakeSlot(int triangleIndex, GameObject gameObject)
        {
            slots[triangleIndex] = gameObject;
        }

        public GameObject GetTowerAt(int position)
        {
            return slots[position];
        }
    }
}
