﻿namespace Assets.GameLogic.Crystals
{
    using Constants;
    using Data.Services;
    using LevelMenu;
    using UnityEngine;

    public class CrystalsLoader : MonoBehaviour
    {
        private LevelService levelService;

        private void Start()
        {
            var levelData = GameObject.Find(FixedGameObjects.ChosenLevelData).GetComponent<ChosenLevelData>();
            levelService = new LevelService(levelData.PackId, levelData.Level);

            var numberOfCrystals = transform.childCount;
            var numberOfLanes = levelService.GetNumberOfLanes();
            if (numberOfCrystals > numberOfLanes)
            {
                for (var i = numberOfLanes; i < numberOfCrystals; i++)
                {
                    Destroy(transform.GetChild(i).gameObject);
                }
            }
        }
    }
}
