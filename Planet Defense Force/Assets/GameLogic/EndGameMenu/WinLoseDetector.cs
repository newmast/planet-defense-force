﻿namespace Assets.GameLogic.EndGameMenu
{
    using Constants;
    using Data.Services;
    using ActionMenu;
    using Components;
    using EnemyLogic;
    using LevelMenu;
    using UnityEngine;
    using Common;

    public class WinLoseDetector : MonoBehaviour {

        [SerializeField]
        private GameObject crystalsParentWrapper;

        [SerializeField]
        private LaneManager laneManager;

        [SerializeField]
        private AnimatedGameMenu endGameMenu;

        private int totalNumberOfCrystals;
        private LevelService levelService;
        private bool gameWon;
        private bool gameOver;

        private void Start()
        {
            var levelData = GameObject.Find(FixedGameObjects.ChosenLevelData).GetComponent<ChosenLevelData>();
            levelService = new LevelService(levelData.PackId, levelData.Level);

            totalNumberOfCrystals = levelService.GetNumberOfLanes();

            foreach (Transform crystal in crystalsParentWrapper.transform)
            {
                var crystalHealth = crystal.GetComponent<Health>();
                crystalHealth.OnDeath = () =>
                {
                    Debug.Log("invoked");
                    totalNumberOfCrystals--;
                    
                    if (totalNumberOfCrystals == 0)
                    {
                        gameOver = true;
                        TouchKit.removeAllGestureRecognizers();
                        ShowEndGameMenu();
                    }
                    else
                    {
                        Destroy(crystalHealth.gameObject);
                    }
                };
            }
        }
        
        private void Update()
        {
            if (gameOver)
            {
                return;
            }

            if (gameWon)
            {
                gameOver = true;
                TouchKit.removeAllGestureRecognizers();
                ShowEndGameMenu();
            }

            gameWon = laneManager.AreAllEnemiesKilled();
        }

        public void ShowEndGameMenu()
        {
            endGameMenu.Show();
            endGameMenu.gameObject
                .GetComponent<EndGameMenuController>()
                .UpdateStatsView();
        }
    }
}
