﻿namespace Assets.GameLogic.EndGameMenu
{
    using System;
    using System.Collections.Generic;
    using Facebook.Unity;
    using UnityEngine;

    public class FacebookController : MonoBehaviour
    {
        private const string Link = "https://play.google.com/store/apps/details?id=com.nick.planetdefenseforce";
        private const string LinkName = "Planet Defense Force";
        private const string LinkCaption = "You can spin around the planet. 10/10";
        private const string LinkDescription = "Fight your way to victory!";
        private const string PictureLink = "https://lh3.googleusercontent.com/Uk0E-W1932zv8jqossn2ndwMou1LwS1JV5szRH-uHcuVUDWhXom2MbNifwIBpVTndA=w300-rw";

        private void Awake()
        {
            if (!FB.IsInitialized)
            {
                FB.Init(InitCallback);
            }
            else
            {
                FB.ActivateApp();
            }
        }

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
        }

        public void ShareToFacebook()
        {
            var perms = new List<string>() { "public_profile", "email", "user_friends" };
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }

        private void AuthCallback(ILoginResult loginResult)
        {
            if (FB.IsLoggedIn)
            {
                FB.FeedShare(
                    link: new Uri(Link),
                    linkName: LinkName,
                    linkCaption: LinkCaption,
                    linkDescription: LinkDescription,
                    picture: new Uri(PictureLink),
                    callback: (result) => { }
                );
            }
            else
            {
                Debug.Log("User cancelled login");
            }
        }
    }
}
