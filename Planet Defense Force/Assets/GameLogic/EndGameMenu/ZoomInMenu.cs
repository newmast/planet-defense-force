﻿namespace Assets.GameLogic.EndGameMenu
{
    using Common;
    using UnityEngine;

    public class ZoomInMenu : AnimatedGameMenu
    {
        [SerializeField]
        private CanvasGroup canvasGroup;

        protected override void OnShow()
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
            Animator.Play("ZoomIn");
        }

        protected override void OnHide()
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            Animator.Play("ZoomOut");
        }
    }
}
