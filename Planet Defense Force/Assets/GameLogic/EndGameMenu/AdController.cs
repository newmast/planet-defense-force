﻿namespace Assets.GameLogic.EndGameMenu
{
    using UnityEngine;
    using UnityEngine.Advertisements;
    using UnityEngine.UI;

    public class AdController : MonoBehaviour
    {
        private const string RewardAdId = "rewardedVideo";

        [SerializeField]
        private Button adButton;

        private void Awake()
        {
            adButton.enabled = Advertisement.IsReady(RewardAdId);
        }

        public void ShowAd()
        {
            if (Advertisement.IsReady(RewardAdId))
            {
                var showOptions = new ShowOptions
                {
                    resultCallback = OnAdWatched
                };

                Advertisement.Show(RewardAdId, showOptions);
            }
        }

        private void OnAdWatched(ShowResult watchResult)
        {
            switch (watchResult)
            {
                case ShowResult.Finished:
                    break;
                case ShowResult.Skipped:
                    break;
                case ShowResult.Failed:
                    break;
            }
        }
    }
}