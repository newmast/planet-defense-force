﻿namespace Assets.GameLogic.EndGameMenu
{
    using Common;
    using Constants;
    using LevelMenu;
    using Resources;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class EndGameMenuController : MonoBehaviour
    {
        [SerializeField]
        private Button restartLevel;

        [SerializeField]
        private Button shop;

        [SerializeField]
        private Button nextLevel;

        [SerializeField]
        private Button mainMenu;

        [SerializeField]
        private Text earnedScoreAmount;

        [SerializeField]
        private Text earnedCoinsAmount;

        private GatheredResources gatheredResources;
        private GameProgressData gameProgressData;

        private void Start()
        {
            gatheredResources = GameObject.FindGameObjectWithTag(Tags.GatheredResources).GetComponent<GatheredResources>();
            gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();

            restartLevel.onClick.AddListener(() =>
            {
                var currentSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(currentSceneBuildIndex);
            });

            nextLevel.onClick.AddListener(() =>
            {
                var levelData = GameObject.Find(FixedGameObjects.ChosenLevelData).GetComponent<ChosenLevelData>();

                if (levelData.Level + 1 <= levelData.MaxLevel)
                {
                    levelData.Level += 1;
                    var currentSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
                    SceneManager.LoadScene(currentSceneBuildIndex);
                }
            });

            shop.onClick.AddListener(() =>
            {
                SceneManager.LoadScene("Scenes/Shop");
            });

            mainMenu.onClick.AddListener(() =>
            {
                SceneManager.LoadScene("Scenes/MainMenu");
            });
        }

        public void UpdateStatsView()
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            var earnedCoins = gatheredResources.Score;
            if (gatheredResources.Score >= 15)
            {
                earnedCoins = (int)(gatheredResources.Score / 4.3f);
            }

            earnedScoreAmount.text = "" + gatheredResources.Score;
            earnedCoinsAmount.text = "" + earnedCoins;

            gameProgressData.Current.playerCoins += earnedCoins;
            gameProgressData.UpdateProgress();
        }
    }
}