﻿namespace Assets.GameLogic.EndGameMenu
{
    using Fabric.Twitter;
    using UnityEngine;

    public class TwitterController : MonoBehaviour
    {
        private void Awake()
        {
            Twitter.Init();
        }

        public void OnButtonClick()
        {
            StartLogin();
        }

        public void StartLogin()
        {
            var session = Twitter.Session;
            if (session == null)
            {
                Twitter.LogIn(LoginComplete, LoginFailure);
            }
            else
            {
                LoginComplete(session);
            }
        }

        public void LoginComplete(TwitterSession session)
        {
            Twitter.RequestEmail(session, RequestEmailComplete);
        }

        private void RequestEmailComplete(string email)
        {
            StartComposer(Twitter.Session);
        }

        public void LoginFailure(ApiError error)
        {
            Debug.Log(string.Format("Error code: {0}. Message: {1}", error.code, error.message));
        }

        public void StartComposer(TwitterSession session)
        {
            Card card = new AppCardBuilder()
                .GooglePlayId("com.nick.planetdefenseforce");

            Twitter.Compose(session, card, new string[] { "#PlanetDefenseForce" });
        }
    }
}
