﻿namespace Assets.GameLogic.Shooting
{
    using Constants;
    using UnityEngine;

    public class EnemyTargeter : MonoBehaviour, ITargeter
    {
        public string TargetTag
        {
            get
            {
                return Tags.Enemy;
            }
        }
    }
}
