﻿namespace Assets.GameLogic.Shooting
{
    using Extensions;
    using Components;
    using Utils;
    using UnityEngine;
    using System;
    public class Shooter : MonoBehaviour
    {
        [SerializeField]
        private Transform bulletSpawnPoint;

        [SerializeField]
        private Collider gameObjectBounds;

        [SerializeField]
        private GameObject bulletPrefab;

        [SerializeField]
        private RangeData rangeComponent;

        [SerializeField]
        private Damage damageComponent;

        [SerializeField]
        private AttackSpeed attackSpeedComponent;

        private bool shouldShoot;
        private float timeSinceLastShot;

        private ITargeter targeter;
        private GameObject target;

        private InvocationSilencer silencer;

        public void MarkTarget(Collider other)
        {
            if (target != null)
            {
                return;
            }

            if (!other.tag.Equals(targeter.TargetTag))
            {
                return;
            }
            
            gameObjectBounds.enabled = false;

            var inLineOfSight = GameplayUtils.IsInLineOfSight(
                bulletSpawnPoint.position,
                other.transform.position,
                rangeComponent.RangeComponent.CurrentRange,
                targeter.TargetTag);
        
            gameObjectBounds.enabled = true;

            if (!inLineOfSight)
            {
                return;
            }

            target = other.gameObject;
        }

        private void Awake()
        {
            silencer = new InvocationSilencer(
                (time) => this.Invoke(silencer.DisableSilence, time));
        }

        private void Start()
        {
            targeter = GetComponent<ITargeter>();
            rangeComponent.RangeComponent.OnTriggerStayAction += (otherCollider) => MarkTarget(otherCollider);
        }

        public GameObject ShootTarget(Vector3 from, GameObject designatedTarget)
        {
            var spawnRotation = Quaternion.LookRotation(designatedTarget.transform.position - from);

            var bullet = Instantiate(bulletPrefab, from, spawnRotation) as GameObject;
            Physics.IgnoreCollision(bullet.GetComponent<Collider>(), gameObjectBounds);

            var bulletDamage = bullet.GetComponent<Damage>();
            bulletDamage.CurrentDamage = damageComponent.CurrentDamage;

            if (OnBulletShot != null)
            {
                OnBulletShot.Invoke(bullet);
            }

            return bullet;
        }
    
        private void Update()
        {
            timeSinceLastShot += Time.deltaTime;
        
            if (target != null && !silencer.IsSilenced())
            {
                if (timeSinceLastShot >= attackSpeedComponent.GetTimeBetweenAttacks())
                {
                    ShootTarget(bulletSpawnPoint.position, target);
                    timeSinceLastShot = 0;
                }
            }
        }

        public InvocationSilencer Silencer
        {
            get
            {
                return silencer;
            }
        }

        public Action<GameObject> OnBulletShot { get; set; }
    }
}
