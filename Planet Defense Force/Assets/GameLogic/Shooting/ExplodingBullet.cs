﻿namespace Assets.GameLogic.Shooting
{
    using Components;
    using EnemyLogic;
    using UnityEngine;
    using Visuals;
    public class ExplodingBullet : MonoBehaviour
    {
        [SerializeField]
        private OnEnemyHitAOE onEnemyHitAOE;

        private float aoeDamage;
        private bool alreadyExploded;

        private void Start()
        {
            onEnemyHitAOE.OnFoundEnemyInRange += (hitEnemy, targetEnemy) =>
            {
                if (!alreadyExploded)
                {
                    alreadyExploded = true;
                    SphereExplosionFactory.CreateCanon(hitEnemy.transform);
                }

                var enemyHealth = targetEnemy.GetComponent<Health>();
                enemyHealth.TakeDamage(GetComponent<Damage>().CurrentDamage / 3f);
            };
        }

        public void SetExplosionParameters(float aoeDamage, float range)
        {
            this.aoeDamage = aoeDamage;
            onEnemyHitAOE.Range = range;
        }
    }
}