﻿namespace Assets.GameLogic.Shooting
{
    public interface ITargeter
    {
        string TargetTag { get; }
    }
}