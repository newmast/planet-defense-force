﻿namespace Assets.GameLogic.Shooting
{
    using System;
    using Components;
    using UnityEngine;

    public class BulletPhysics : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody bulletBody;

        [SerializeField]
        private Damage damageComponent;

        private void DestroyBullet()
        {
            Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            var otherHealthComponent = other.gameObject.GetComponentInParent<Health>();
            if (otherHealthComponent != null)
            {
                otherHealthComponent.TakeDamage(damageComponent.CurrentDamage);

                if (OnBulletDestroyed != null)
                {
                    OnBulletDestroyed.Invoke(other);
                }

                DestroyBullet();
            }
        }

        private Vector3 GetInitialForce()
        {
            return transform.forward * 2f;
        }

        private void Start()
        {
            bulletBody.AddForce(GetInitialForce(), ForceMode.Impulse);
            Invoke("DestroyBullet", 15);
        }

        public Action<Collision> OnBulletDestroyed { get; set; }
    }
}
