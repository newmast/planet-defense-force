﻿namespace Assets.GameLogic.Shooting
{
    using EnemyLogic;
    using UnityEngine;

    public class EmpBullet : MonoBehaviour
    {
        [SerializeField]
        private BulletPhysics bulletPhysics;

        private void Start()
        {
            // TODO: Refactor
            bulletPhysics.OnBulletDestroyed += (hitEnemyCollider) =>
            {
                var enemyShooter = hitEnemyCollider.transform.parent.GetComponent<Shooter>();
                if (enemyShooter != null)
                {
                    enemyShooter.Silencer.Silence(SilenceTime);
                }

                var enemyHealing = hitEnemyCollider.transform.parent.GetComponent<HealNearbyEnemies>();
                if (enemyHealing != null)
                {
                    enemyHealing.Silencer.Silence(SilenceTime);
                }
            };

        }

        public float SilenceTime { get; set; }
    }
}
