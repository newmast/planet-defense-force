﻿namespace Assets.GameLogic.Shooting
{
    using EnemyLogic;
    using UnityEngine;

    public class BounceBullet : MonoBehaviour
    {
        [SerializeField]
        private OnEnemyHitAOE onEnemyHitAOE;

        private void Start()
        {
            onEnemyHitAOE.OnFoundEnemyInRange += (hitEnemy, targetEnemy) =>
            {
                if (hitEnemy == targetEnemy)
                {
                    return;
                }

                var enemyShooter = hitEnemy.GetComponent<Shooter>();
                if (enemyShooter != null && BouncesLeft > 0)
                {
                    var bullet = enemyShooter.ShootTarget(hitEnemy.transform.position, targetEnemy);
                    bullet.transform.GetComponent<BounceBullet>().BouncesLeft = BouncesLeft - 1;
                }
            };
        }

        public int BouncesLeft { get; set; }
    }
}
