﻿namespace Assets.GameLogic.Shooting
{
    using Constants;
    using UnityEngine;

    public class CrystalTargeter : MonoBehaviour, ITargeter
    {
        public string TargetTag
        {
            get
            {
                return Tags.Crystal;
            }
        }
    }
}
