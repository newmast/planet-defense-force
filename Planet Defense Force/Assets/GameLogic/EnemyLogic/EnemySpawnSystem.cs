﻿namespace Assets.GameLogic.EnemyLogic
{
    using System;
    using Constants;
    using Components;
    using UnityEngine;
    using InfoMessages;
    public class EnemySpawnSystem : MonoBehaviour
    {
        public Action OnGroupSpawned { get; set; }

        public GameObject SpawnEnemyGroup(string enemyGroupPrefabPath)
        {
            var parentContainer = new GameObject("Enemy group");
            parentContainer.tag = Tags.EnemyGroup;
            parentContainer.layer = LayerMask.NameToLayer("Ignore Raycast");

            var parentRangeComponent = parentContainer.AddComponent<Range>();
            var parentSphereComponent = parentContainer.AddComponent<SphereCollider>();
            var parentRigidbody = parentContainer.AddComponent<Rigidbody>();

            parentSphereComponent.radius = 1;
            parentSphereComponent.isTrigger = true;
            parentSphereComponent.enabled = true;

            parentRigidbody.useGravity = false;
            parentRigidbody.isKinematic = true;

            parentContainer.AddComponent<EnemyGroupMovement>().enabled = true;

            var enemyPlaceholderGroup = Resources.Load(enemyGroupPrefabPath) as GameObject;
            var placeholders = enemyPlaceholderGroup.GetComponentsInChildren<EnemyPlaceholder>();

            foreach (var placeholder in placeholders)
            {
                var prefabObject = Instantiate(
                    placeholder.EnemyPrefab, 
                    placeholder.transform.position, 
                    placeholder.transform.rotation) as GameObject;

                var rangeObject = prefabObject.transform.Find("RangeCollider").gameObject;
                if (rangeObject != null)
                {
                    var rangeData = rangeObject.GetComponent<RangeData>();

                    rangeData.RangeComponent = parentRangeComponent;
                    rangeData.SphereComponent = parentSphereComponent;
                }

                prefabObject.transform.SetParent(parentContainer.transform, false);
            }

            parentContainer.AddComponent<EnemyGroupController>();

            if (OnGroupSpawned != null)
            {
                OnGroupSpawned.Invoke();
            }

            InfoMessageFactory.CreateToast("Enemies incoming", true);

            return parentContainer;
        }
    }
}
