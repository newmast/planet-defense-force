﻿namespace Assets.GameLogic.EnemyLogic
{
    using Assets.Data.Models.Enemies;
    using System;

    [Serializable]
    public class WeakEnemyDataModel : BaseEnemyDataModel
    {
        public float damage;
    }
}
