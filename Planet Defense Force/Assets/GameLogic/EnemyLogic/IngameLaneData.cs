﻿namespace Assets.GameLogic.EnemyLogic
{
    using System.Collections.Generic;
    using UnityEngine;

    public class IngameLaneData
    {
        public List<GameObject> Enemies { get; set; }
        
        public float TimeRequiredForSpawn { get; set; }

        public float TimeWaited { get; set; }

        public int CurrentGroupIndexInLane { get; set; }

        public Transform LaneCrystal { get; set; }

        public bool IsLaneClosed { get; set; }
    }
}
