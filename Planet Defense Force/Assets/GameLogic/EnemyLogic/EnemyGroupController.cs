﻿namespace Assets.GameLogic.EnemyLogic
{
    using Components;
    using UnityEngine;

    public class EnemyGroupController : MonoBehaviour
    {
        private void Start()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                var enemy = transform.GetChild(i);

                var enemyRangeCollider = enemy.Find("RangeCollider");
                var hasRangeCollider = enemyRangeCollider != null;

                if (hasRangeCollider)
                {
                    var rangeComponent = enemyRangeCollider.GetComponent<Range>();
                    rangeComponent.Collider = GetComponent<SphereCollider>();
                }
            }
        }

        public void Update()
        {
            if (transform.childCount == 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
