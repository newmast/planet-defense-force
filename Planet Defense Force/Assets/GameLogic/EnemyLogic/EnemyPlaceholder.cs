﻿namespace Assets.GameLogic.EnemyLogic
{
    using System.Collections.Generic;
#if UNITY_EDITOR
    using UnityEditor;
    using UnityEditor.Callbacks;
#endif
    using UnityEngine;

    [ExecuteInEditMode]
    public class EnemyPlaceholder : MonoBehaviour
    {
        [SerializeField]
        private GameObject prefab;

#if UNITY_EDITOR
        // Struct of all components. Used for edit-time visualization and gizmo drawing
        public struct Thingy
        {
            public Mesh mesh;
            public Matrix4x4 matrix;
            public List<Material> materials;
        }

        [System.NonSerialized]
        public List<Thingy> things = new List<Thingy>();

        private void OnValidate()
        {
            things.Clear();
            if (enabled)
            {
                Rebuild(prefab, Matrix4x4.identity);
            }
        }

        private void OnEnable()
        {
            things.Clear();
            if (enabled)
            {
                Rebuild(prefab, Matrix4x4.identity);
            }
        }

        private void Rebuild(GameObject source, Matrix4x4 inMatrix)
        {
            if (!source)
            {
                return;
            }

            var baseMat = inMatrix * Matrix4x4.TRS(-source.transform.position, Quaternion.identity, Vector3.one);

            foreach (Renderer mr in source.GetComponentsInChildren(typeof(Renderer), true))
            {
                var mrMeshFilter = mr.GetComponent<MeshFilter>();
                if (mrMeshFilter == null)
                {
                    continue;
                }

                things.Add(new Thingy
                {
                    mesh = mrMeshFilter.sharedMesh,
                    matrix = baseMat * mr.transform.localToWorldMatrix,
                    materials = new List<Material>(mr.sharedMaterials)
                });
            }

            foreach (EnemyPlaceholder pi in source.GetComponentsInChildren(typeof(EnemyPlaceholder), true))
            {
                if (pi.enabled && pi.gameObject.activeSelf)
                {
                    Rebuild(pi.prefab, baseMat * pi.transform.localToWorldMatrix);
                }
            }
        }

        // Editor-time-only update: Draw the meshes so we can see the objects in the scene view
        private void Update()
        {
            if (EditorApplication.isPlaying)
            {
                return;
            }
            var mat = transform.localToWorldMatrix;
            foreach (var t in things)
            {
                for (var i = 0; i < t.materials.Count; i++)
                {
                    Graphics.DrawMesh(t.mesh, mat * t.matrix, t.materials[i], gameObject.layer, null, i);
                }
            }
        }

        // Picking logic: Since we don't have gizmos.drawmesh, draw a bounding cube around each thingy
        private void OnDrawGizmos() { DrawGizmos(new Color(0, 0, 0, 0)); }
        private void OnDrawGizmosSelected() { DrawGizmos(new Color(0, 0, 1, .2f)); }

        private void DrawGizmos(Color col)
        {
            if (EditorApplication.isPlaying)
            {
                return;
            }
            Gizmos.color = col;
            var mat = transform.localToWorldMatrix;
            foreach (var t in things)
            {
                Gizmos.matrix = mat * t.matrix;
                Gizmos.DrawCube(t.mesh.bounds.center, t.mesh.bounds.size);
            }
        }

        // Baking stuff: Copy in all the referenced objects into the scene on play or build
        [PostProcessScene(-2)]
        public static void OnPostprocessScene()
        {
            foreach (EnemyPlaceholder pi in FindObjectsOfType(typeof(EnemyPlaceholder)))
            {
                BakeInstance(pi);
            }
        }

        public static void BakeInstance(EnemyPlaceholder pi)
        {
            if (!pi.prefab || !pi.enabled)
            {
                return;
            }
            pi.enabled = false;
            var go = PrefabUtility.InstantiatePrefab(pi.prefab) as GameObject;
            var rot = go.transform.localRotation;
            var scale = go.transform.localScale;
            go.transform.parent = pi.transform;
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = scale;
            go.transform.localRotation = rot;
            pi.prefab = null;
            foreach (var childPi in go.GetComponentsInChildren<EnemyPlaceholder>())
            {
                BakeInstance(childPi);
            }
        }

#endif

        public GameObject EnemyPrefab
        {
            get
            {
                return prefab;
            }
        }
    }
}
