﻿namespace Assets.GameLogic.EnemyLogic
{
    using System;
    using System.Linq;
    using Constants;
    using Shooting;
    using UnityEngine;

    public class OnEnemyHitAOE : MonoBehaviour
    {
        [SerializeField]
        private BulletPhysics bulletPhysics;
    
        private void Start()
        {
            bulletPhysics.OnBulletDestroyed += (collision) =>
            {
                var enemyBulletHitCollider = collision.gameObject;
                if (enemyBulletHitCollider.tag != Tags.Enemy)
                {
                    return;
                }

                var enemyBulletHit = enemyBulletHitCollider.transform.parent.gameObject;
                var nearbyEnemiesBounds = Physics
                    .OverlapSphere(enemyBulletHit.transform.position, Range)
                    .Where(x => x.tag == Tags.Enemy)
                    .ToArray();

                foreach (var nearbyEnemyBounds in nearbyEnemiesBounds)
                {
                    var nearbyEnemy = nearbyEnemyBounds.transform.parent.gameObject;
                    OnFoundEnemyInRange.Invoke(enemyBulletHit, nearbyEnemy);
                }
            };
        }

        public float Range { get; set; }

        public Action<GameObject, GameObject> OnFoundEnemyInRange { get; set; }
    }
}