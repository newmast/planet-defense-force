﻿namespace Assets.GameLogic.EnemyLogic
{
    using Constants;
    using LevelMenu;
    using UnityEngine;
    using Data.Services;
    using System.Collections.Generic;
    using System;

    public class LaneManager : MonoBehaviour
    {
        private const float SpawnDistanceFactor = 2f;

        [SerializeField]
        private EnemySpawnSystem enemySpawnSystem;

        [SerializeField]
        private Transform crystalsContainer;

        private Transform planetTransform;

        private IngameLaneData[] gameLanesData;
        private int numberOfLanes;
        private int totalNumberOfEnemies;
        private int spawnedEnemiesCount;
        private int currentlyDistributedLaneIndex;

        private LevelService service;
    
        private void Start()
        {
            planetTransform = GameObject.FindGameObjectWithTag(Tags.Planet).transform;

            var levelData = GameObject.Find(FixedGameObjects.ChosenLevelData).GetComponent<ChosenLevelData>();
            service = new LevelService(levelData.PackId, levelData.Level);
            numberOfLanes = service.GetNumberOfLanes();

            gameLanesData = new IngameLaneData[numberOfLanes];
            for (var laneIndex = 0; laneIndex < numberOfLanes; laneIndex++)
            {
                gameLanesData[laneIndex] = new IngameLaneData
                {
                    Enemies = new List<GameObject>(),
                    TimeRequiredForSpawn = service.GetSpawnTimeInMs(laneIndex, 0) / 1000f,
                    LaneCrystal = crystalsContainer.GetChild(laneIndex).transform
                };

                totalNumberOfEnemies += service.GetNumberOfEnemies(laneIndex);
            }
        }
        
        private void Update()
        {
            for (var laneIndex = 0; laneIndex < numberOfLanes; laneIndex++)
            {
                var laneData = gameLanesData[laneIndex];
                
                if (laneData.LaneCrystal == null)
                {
                    laneData.IsLaneClosed = true;
                }
            }

            for (var laneIndex = 0; laneIndex < numberOfLanes; laneIndex++)
            {
                var laneData = gameLanesData[laneIndex];
                laneData.TimeWaited += Time.deltaTime;

                if (laneData.IsLaneClosed)
                {
                    if (laneData.Enemies.Count > 0)
                    {
                        DistributeEnemyGroupsAcrossAllLanes(laneIndex);
                    }

                    return;
                }

                if (laneData.Enemies.Count > 0 && laneData.Enemies[0] == null)
                {
                    laneData.Enemies.RemoveAt(0);
                }

                RelocateEnemies(laneData);

                if (!service.AnyLeft(laneIndex, laneData.CurrentGroupIndexInLane))
                {
                    continue;
                }

                if (laneData.TimeWaited >= laneData.TimeRequiredForSpawn)
                {
                    SpawnEnemyGroupFromService(laneIndex);
                }
            }
        }

        private void SpawnEnemyGroupFromService(int laneIndex)
        {
            var enemyGroup = enemySpawnSystem.SpawnEnemyGroup(
                GetCurrentPrefabPath(laneIndex));

            var laneData = gameLanesData[laneIndex];

            if (laneData.IsLaneClosed)
            {
                return;
            }

            laneData.CurrentGroupIndexInLane++;
            laneData.TimeRequiredForSpawn =
                service.GetSpawnTimeInMs(laneIndex, laneData.CurrentGroupIndexInLane) / 1000f;
            laneData.TimeWaited = 0;

            spawnedEnemiesCount++;

            EnqueueEnemyGroupInLane(laneIndex, enemyGroup);
        }

        private void EnqueueEnemyGroupInLane(int laneIndex, GameObject enemyGroup)
        {
            var laneData = gameLanesData[laneIndex];

            laneData.Enemies.Add(enemyGroup);

            var spawnDirection = laneData.LaneCrystal.position - planetTransform.position;
            int spawnOffsetIndices = 5;
            var position = spawnDirection * (laneData.Enemies.Count + spawnOffsetIndices) * SpawnDistanceFactor;
            var orientation = Quaternion.LookRotation(planetTransform.position - position);

            enemyGroup.transform.position = position;
            enemyGroup.transform.rotation = orientation;
        }

        private void DistributeEnemyGroupAcrossAppropriateLane(GameObject enemyGroup)
        {
            currentlyDistributedLaneIndex = (currentlyDistributedLaneIndex + 1) % gameLanesData.Length;
            bool lookingForAvailableLane = true;
            while (lookingForAvailableLane)
            {
                if (gameLanesData[currentlyDistributedLaneIndex].IsLaneClosed)
                {
                    currentlyDistributedLaneIndex = (currentlyDistributedLaneIndex + 1) % gameLanesData.Length;
                }
                else
                {
                    lookingForAvailableLane = false;
                }
            }

            EnqueueEnemyGroupInLane(currentlyDistributedLaneIndex, enemyGroup);
            RelocateEnemies(gameLanesData[currentlyDistributedLaneIndex]);
        }

        private void DistributeEnemyGroupsAcrossAllLanes(int fromLaneIndex)
        {
            var laneData = gameLanesData[fromLaneIndex];
            for (var i = 0; i < laneData.Enemies.Count; i++)
            {
                var enemyGroup = laneData.Enemies[i];
                laneData.Enemies.Remove(enemyGroup);
                DistributeEnemyGroupAcrossAppropriateLane(enemyGroup);
            }
        }

        public bool IsLaneClosed(int laneIndex)
        {
            return gameLanesData[laneIndex].IsLaneClosed;
        }

        public bool AreAllEnemiesKilled()
        {
            var allLanesEmpty = true;
            for (var i = 0; i < service.GetNumberOfLanes(); i++)
            {
                allLanesEmpty = allLanesEmpty && gameLanesData[i].Enemies.Count == 0;
            }

            return spawnedEnemiesCount == totalNumberOfEnemies && allLanesEmpty;
        }

        private void RelocateEnemies(IngameLaneData data)
        {
            var spawnDirection = data.LaneCrystal.position - planetTransform.position;
            var distanceBetweenGroups = spawnDirection * SpawnDistanceFactor;

            var currentDistance = distanceBetweenGroups;
            for (var i = 0; i < data.Enemies.Count; i++)
            {
                var enemyGroup = data.Enemies[i];
                var movement = enemyGroup.GetComponent<EnemyGroupMovement>();

                movement.Move(currentDistance);
                currentDistance += distanceBetweenGroups;
            }
        }

        private string GetCurrentPrefabPath(int laneIndex)
        {
            var laneData = gameLanesData[laneIndex];
            var prefabName = service.GetGroupTag(laneIndex, laneData.CurrentGroupIndexInLane);

            return DataPaths.EnemyGroupsPrefabs + prefabName;
        }
    }
}
