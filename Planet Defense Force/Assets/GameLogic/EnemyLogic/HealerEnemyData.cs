﻿namespace Assets.GameLogic.EnemyLogic
{
    using Constants;

    public class HealerEnemyData : BaseEnemyData
    {
        public HealerEnemyData()
            : base(DataPaths.HealerEnemy)
        {
        }
    }
}
