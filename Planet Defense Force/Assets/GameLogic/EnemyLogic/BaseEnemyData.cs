﻿namespace Assets.GameLogic.EnemyLogic
{
    using Data.Models.Enemies;
    using Data.Services;
    using Components;
    using UnityEngine;

    public abstract class BaseEnemyData : MonoBehaviour
    {
        [SerializeField]
        private RangeData range;

        [SerializeField]
        private AttackSpeed attackSpeed;

        [SerializeField]
        private Health health;

        [SerializeField]
        private EnemyDeath enemyDeath;

        private string dataPath;
        private BaseEnemyDataModel model;

        protected string DataPath
        {
            get
            {
                return dataPath;
            }
        }

        public BaseEnemyData(string enemyDataPath)
        {
            dataPath = enemyDataPath;
        }

        public virtual void Start()
        {
            model = new DataValuesService().GetEnemyDataModel<BaseEnemyDataModel>(dataPath);

            range.RangeComponent.CurrentRange = model.range;
            attackSpeed.CurrentAttackSpeed = model.attackSpeed;
            health.MaxHealth = model.maxHealth;
            health.CurrentHealth = model.maxHealth;
            enemyDeath.GoldWorth = model.goldWorth;
            enemyDeath.ScoreWorth = model.scoreWorth;
        }
    }
}
