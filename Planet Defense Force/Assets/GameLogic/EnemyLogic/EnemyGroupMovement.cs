﻿namespace Assets.GameLogic.EnemyLogic
{
    using Constants;
    using UnityEngine;

    public class EnemyGroupMovement : MonoBehaviour
    {
        private float speed;

        private SphereCollider rangeCollider;

        private Vector3 destination;
        private Transform planetTransform;
        private bool shouldMove;

        private void Awake()
        {
            speed = 2;
            rangeCollider = GetComponent<SphereCollider>();
        }

        private void Start()
        {
            planetTransform = GameObject.FindGameObjectWithTag(Tags.Planet).transform;
        }

        public void Move(Vector3 endPoint)
        {
            shouldMove = true;
            destination = endPoint; 
        }
	
        private void Update()
        {
            if (shouldMove)
            {
                if (!IsInRange())
                {
                    transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
                }
                else
                {
                    shouldMove = false;
                }
            }
        }

        private bool IsInRange()
        {
            return Vector3.Distance(transform.position, planetTransform.position) <= rangeCollider.radius;
        }
    }
}