﻿namespace Assets.GameLogic.EnemyLogic
{
    using Constants;
    using UnityEngine;

    public class TankEnemyData : BaseEnemyData
    {
        public TankEnemyData()
            : base(DataPaths.TankEnemy)
        {
        }
    }
}
