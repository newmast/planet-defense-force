﻿namespace Assets.GameLogic.EnemyLogic
{
    using System.Linq;
    using Constants;
    using Extensions;
    using Components;
    using UnityEngine;
    using Utils;
    using Visuals;
    public class HealNearbyEnemies : MonoBehaviour
    {
        private const float HealingInterval = 5f;
        private InvocationSilencer silencer;

        private void Awake()
        {
            silencer = new InvocationSilencer(
                (time) => this.Invoke(() => silencer.DisableSilence(), time));
        }

        private void Start()
        {
            this.InvokeRepeating(HealingWave, 0, HealingInterval);
        }

        private void HealingWave()
        {
            if (silencer.IsSilenced())
            {
                return;
            }

            var nearbyEnemies = Physics
                .OverlapSphere(transform.position, 1f)
                .Where(x => x.tag == Tags.Enemy)
                .ToArray();

            foreach (var enemy in nearbyEnemies)
            {
                var enemyHealth = enemy.GetComponentInParent<Health>();

                if (enemy.gameObject != gameObject &&enemyHealth != null)
                {
                    enemyHealth.Heal(5);
                }
            }

            SphereExplosionFactory.CreateHealing(transform);
        }
        
        public InvocationSilencer Silencer
        {
            get
            {
                return silencer;
            }
        }
    }
}
