﻿namespace Assets.GameLogic.EnemyLogic
{
    using Constants;
    using Components;
    using Resources;
    using UnityEngine;

    public class EnemyDeath : MonoBehaviour
    {
        [SerializeField]
        private Health healthComponent;

        [SerializeField]
        private Transform deathParticle;

        private GatheredResources gatheredResources;

        private void Start()
        {
            gatheredResources = GameObject.FindGameObjectWithTag(Tags.GatheredResources).GetComponent<GatheredResources>();

            healthComponent.OnDeath += () =>
            {
                gatheredResources.AddGold(GoldWorth);
                gatheredResources.AddScore(ScoreWorth);

                if (deathParticle != null)
                {
                    var particle = Instantiate(deathParticle, transform.position, Quaternion.identity) as Transform;
                    var particleSystem = particle.gameObject.GetComponent<ParticleSystem>();

                    particleSystem.Play();
                }

                Destroy(gameObject);
            };
        }

        private void OnDestroy()
        {
            var enemyAudio = GetComponent<AudioSource>();
            Debug.Log(enemyAudio);
            if (enemyAudio == null)
            {
                return;
            }

            GameObject deathAudio = new GameObject("Death audio");
            deathAudio.transform.position = transform.position;
            deathAudio.transform.parent = transform.parent;

            var audioSource = deathAudio.AddComponent<AudioSource>();
            audioSource.clip = GetComponent<AudioSource>().clip;

            audioSource.volume = 0.01f;
            audioSource.Play();

            Destroy(deathAudio, audioSource.clip.length + 1);
        }

        public int GoldWorth { get; set; }

        public int ScoreWorth { get; set; }
    }
}
