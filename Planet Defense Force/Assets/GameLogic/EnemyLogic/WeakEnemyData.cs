﻿namespace Assets.GameLogic.EnemyLogic
{
    using Components;
    using Constants;
    using Data.Services;
    using UnityEngine;

    public class WeakEnemyData : BaseEnemyData
    {
        [SerializeField]
        private Damage damage;

        public WeakEnemyData()
            : base(DataPaths.WeakEnemy)
        {
        }

        public override void Start()
        {
            base.Start();

            damage.CurrentDamage = new DataValuesService()
                .GetEnemyDataModel<WeakEnemyDataModel>(DataPath).damage;
        }
    }
}
