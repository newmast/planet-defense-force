﻿namespace Assets.GameLogic.Actions
{
    using System;
    using System.Collections.Generic;
    using Components;
    using Data.Models;
    using Data.Models.OtherActions;
    using Data.Models.Upgrades;
    using Data.Services;
    using Upgrading;
    using Utils;
    using UnityEngine;

    public class CrystalActionMenuData : MonoBehaviour, IActionMenuData
    {
        [SerializeField]
        private Health healthComponent;

        private ActionItemData actionData;
        private AllDataValues<HealingModel> model;

        private void Start()
        {
            actionData = new ActionsService().GetCrystalHealingActionModel();
            model = new DataValuesService().GetCrystalHealingData();

            healthComponent.MaxHealth = 600;
            healthComponent.CurrentHealth = healthComponent.MaxHealth;
        }

        public List<ActionButtonViewModel> GetActionMenuData()
        {
            Action<int> healCrystal = 
                (level) => healthComponent.Heal(ModelUtils.GetLevelDetails(model, level).health);

            return new List<ActionButtonViewModel>
            {
                ActionButtonViewModelFactory.CreateAction(actionData, 1, () => healCrystal.Invoke(1)),
                ActionButtonViewModelFactory.CreateAction(actionData, 2, () => healCrystal.Invoke(2)),
                ActionButtonViewModelFactory.CreateAction(actionData, 3, () => healCrystal.Invoke(3))
            };
        }
    }
}
