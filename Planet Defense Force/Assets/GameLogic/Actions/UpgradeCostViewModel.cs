﻿namespace Assets.GameLogic.Actions
{
    public class UpgradeCostViewModel
    {
        public int Wood { get; set; }

        public int Titanium { get; set; }

        public int Gold { get; set; }
    }
}