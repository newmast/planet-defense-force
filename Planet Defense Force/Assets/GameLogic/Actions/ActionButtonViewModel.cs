﻿namespace Assets.GameLogic.Actions
{
    using System;

    public class ActionButtonViewModel
    {
        public int Level { get; set; }

        public string Title { get; set; }

        public string ThumbnailUrl { get; set; }
        
        public UpgradeCostViewModel UpgradeCost { get; set; }

        public Action OnClick { get; set; }
    }
}
