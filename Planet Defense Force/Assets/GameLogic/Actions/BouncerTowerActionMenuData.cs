﻿
namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;
    using Components;
    using Constants;
    using Upgrading.UpgradeableComponents;
    using UnityEngine;
    using Upgrading;
    using Data.Services;
    public class BouncerTowerActionMenuData : MonoBehaviour, IUpgradeActionsMenuData
    {
        [SerializeField]
        private Damage damage;

        [SerializeField]
        private RangeData range;

        [SerializeField]
        private AttackSpeed attackSpeed;

        [SerializeField]
        private BounceCount bounceCount;

        public List<IUpgrade> GetUpgradeModels(TowerUpgradeManager tum)
        {
            return new List<IUpgrade>
            {
                new BounceCountUpgrade(tum, bounceCount, DataPaths.BouncerTowerData),
                new DamageUpgrade(tum, damage, DataPaths.BouncerTowerData),
                new RangeUpgrade(tum, range, DataPaths.BouncerTowerData),
                new AttackSpeedUpgrade(tum, attackSpeed, DataPaths.BouncerTowerData)
            };
        }
    }
}
