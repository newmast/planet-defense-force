﻿
namespace Assets.GameLogic.Actions
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using Data.Models;
    using Data.Models.OtherActions;
    using Data.Models.Upgrades;
    using Data.Services;
    using Resources;
    using Utils;
    using UnityEngine;

    public class TitaniumShopActionMenuData : MonoBehaviour, IActionMenuData
    {
        private ActionItemData actionData;
        private AllDataValues<TitaniumShopModel> model;
        private GatheredResources resources;

        private void Start()
        {
            model = new DataValuesService().GetTitaniumShopDataModel();
            actionData = new ActionsService().GetTitaniumShopActionModel();
            resources = GameObject.FindGameObjectWithTag(Tags.GatheredResources).GetComponent<GatheredResources>();
        }

        public List<ActionButtonViewModel> GetActionMenuData()
        {
            Action<int> purchaseTitanium = (level) =>
            {
                var titaniumToAdd = ModelUtils.GetLevelDetails(model, level).titanium;
                resources.AddTitanium(titaniumToAdd);
            };

            return new List<ActionButtonViewModel>
            {
                ActionButtonViewModelFactory.CreateAction(actionData, 1, () => purchaseTitanium.Invoke(1)),
                ActionButtonViewModelFactory.CreateAction(actionData, 2, () => purchaseTitanium.Invoke(2)),
                ActionButtonViewModelFactory.CreateAction(actionData, 3, () => purchaseTitanium.Invoke(3)),
                ActionButtonViewModelFactory.CreateAction(actionData, 4, () => purchaseTitanium.Invoke(4))
            };
        }
    }
}
