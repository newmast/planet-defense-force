﻿namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;
    using Components;
    using Upgrading.UpgradeableComponents;
    using UnityEngine;
    using Upgrading;

    public class WoodTowerActionMenuData : MonoBehaviour, IUpgradeActionsMenuData
    {
        [SerializeField]
        private Wood woodComponent;

        public List<IUpgrade> GetUpgradeModels(TowerUpgradeManager tum)
        {
            return new List<IUpgrade>
            {
                new WoodUpgrade(tum, woodComponent)
            };
        }
    }
}
