﻿
namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;
    using Components;
    using Constants;
    using Upgrading.UpgradeableComponents;
    using UnityEngine;
    using Upgrading;

    public class CannonTowerActionMenuData : MonoBehaviour, IUpgradeActionsMenuData
    {
        [SerializeField]
        private Damage damage;

        [SerializeField]
        private RangeData range;

        [SerializeField]
        private AttackSpeed attackSpeed;

        [SerializeField]
        private ExplosionRadius explosionRadius;

        public List<IUpgrade> GetUpgradeModels(TowerUpgradeManager tum)
        {
            return new List<IUpgrade>
            {
                new ExplosionRadiusUpgrade(tum, explosionRadius, DataPaths.CannonTowerData),
                new DamageUpgrade(tum, damage, DataPaths.CannonTowerData),
                new RangeUpgrade(tum, range, DataPaths.CannonTowerData),
                new AttackSpeedUpgrade(tum, attackSpeed, DataPaths.CannonTowerData)
            };
        }
    }
}