﻿namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;
    using Components;
    using Constants;
    using Upgrading.UpgradeableComponents;
    using UnityEngine;
    using Upgrading;

    public class EmpTowerActionMenuData : MonoBehaviour, IUpgradeActionsMenuData
    {
        [SerializeField]
        private Damage damage;

        [SerializeField]
        private RangeData range;

        [SerializeField]
        private AttackSpeed attackSpeed;

        [SerializeField]
        private Silence silence;

        private TowerUpgradeManager towerUpgradeManager;

        public List<IUpgrade> GetUpgradeModels(TowerUpgradeManager tum)
        {
            return new List<IUpgrade>
            {
                new SilenceUpgrade(tum, silence, DataPaths.EmpTowerData),
                new DamageUpgrade(tum, damage, DataPaths.EmpTowerData),
                new RangeUpgrade(tum, range, DataPaths.EmpTowerData),
                new AttackSpeedUpgrade(tum, attackSpeed, DataPaths.EmpTowerData)
            };
        }
    }
}