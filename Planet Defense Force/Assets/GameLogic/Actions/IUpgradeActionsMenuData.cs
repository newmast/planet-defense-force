﻿namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;
    using Upgrading;
    using Upgrading.UpgradeableComponents;

    public interface IUpgradeActionsMenuData
    {
        List<IUpgrade> GetUpgradeModels(TowerUpgradeManager tum);
    }
}