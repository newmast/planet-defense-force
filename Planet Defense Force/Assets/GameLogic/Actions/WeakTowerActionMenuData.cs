﻿namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;
    using Components;
    using Constants;
    using Upgrading.UpgradeableComponents;
    using UnityEngine;
    using Upgrading;

    public class WeakTowerActionMenuData : MonoBehaviour, IUpgradeActionsMenuData
    {
        [SerializeField]
        private Damage damage;

        [SerializeField]
        private RangeData range;

        [SerializeField]
        private AttackSpeed attackSpeed;

        public void Start()
        {
        }

        public List<IUpgrade> GetUpgradeModels(TowerUpgradeManager tum)
        {
            return new List<IUpgrade>
            {
                new DamageUpgrade(tum, damage, DataPaths.WeakTowerData),
                new RangeUpgrade(tum, range, DataPaths.WeakTowerData),
                new AttackSpeedUpgrade(tum, attackSpeed, DataPaths.WeakTowerData)
            };
        }
    }
}