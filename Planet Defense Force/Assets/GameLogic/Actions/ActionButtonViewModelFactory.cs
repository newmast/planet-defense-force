﻿namespace Assets.GameLogic.Actions
{
    using Utils;
    using System;
    using Data.Models;
    using Upgrading;
    using Upgrading.UpgradeableComponents;

    public static class ActionButtonViewModelFactory
    {
        private static UpgradeCostViewModel GetUpgradeCost(ActionItemData model, int level)
        {
            return new UpgradeCostViewModel
            {
                Gold = ModelUtils.GetActionModelDetails(model, level).cost.gold,
                Titanium = ModelUtils.GetActionModelDetails(model, level).cost.titanium,
                Wood = ModelUtils.GetActionModelDetails(model, level).cost.wood
            };
        }

        public static ActionButtonViewModel CreateUpgradeAction(TowerUpgradeManager towerUpgradeManager, IUpgrade upgrade)
        {
            var level = towerUpgradeManager.GetLevel(upgrade.GetComponent());
            var actionData = upgrade.GetActionItemData();

            return new ActionButtonViewModel
            {
                Title = actionData.title,
                ThumbnailUrl = actionData.backgroundUrl,
                UpgradeCost = GetUpgradeCost(actionData, level),
                OnClick = () => towerUpgradeManager.Upgrade(upgrade)
            };
        }

        public static ActionButtonViewModel CreateAction(ActionItemData actionData, int level, Action onClick)
        {
            return new ActionButtonViewModel
            {
                Title = actionData.title,
                ThumbnailUrl = actionData.backgroundUrl,
                UpgradeCost = GetUpgradeCost(actionData, level),
                OnClick = onClick
            };
        }
    }
}
