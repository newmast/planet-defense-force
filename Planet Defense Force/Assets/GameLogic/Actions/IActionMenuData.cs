﻿namespace Assets.GameLogic.Actions
{
    using System.Collections.Generic;

    public interface IActionMenuData
    {
        List<ActionButtonViewModel> GetActionMenuData();
    }
}