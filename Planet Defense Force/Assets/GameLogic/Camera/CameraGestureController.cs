﻿namespace Assets.GameLogic.Camera
{
    using System;
    using Constants;
    using UnityEngine;

    public class CameraGestureController : MonoBehaviour
    {
        [SerializeField]
        private PauseButton pauseManager;

        [SerializeField]
        private float swipeVelocityCoefficient;

        [SerializeField]
        private float minStoppingVelocity;

        [SerializeField]
        private float velocityDeclineCoefficient;

        [SerializeField]
        private float minPlanetDistance;

        [SerializeField]
        private float maxPlanetDistance;

        [SerializeField]
        private float zoomSpeed;

        [SerializeField]
        private int rotationSpeedCoeffiecient;

        private Transform planetTransform;

        private float cameraVelocity;
        private float accumulatedRotation;
        private Vector3 cameraDirection;

        private void Start()
        {
            cameraDirection = new Vector3();

            var panRecongizer = new TKPanRecognizer();
            panRecongizer.gestureRecognizedEvent += OnPan;

            var rotationRecognizer = new TKRotationRecognizer();
            rotationRecognizer.minimumRotationToRecognize = 1f;
            rotationRecognizer.gestureRecognizedEvent += OnRotate;

            var pinchRecognizer = new TKPinchRecognizer();
            pinchRecognizer.minimumScaleDistanceToRecognize = 1f;
            pinchRecognizer.gestureRecognizedEvent += OnPinch;

            TouchKit.addGestureRecognizer(panRecongizer);
            TouchKit.addGestureRecognizer(rotationRecognizer);
            TouchKit.addGestureRecognizer(pinchRecognizer);

            planetTransform = GameObject.FindGameObjectWithTag(Tags.Planet).transform;
        }

        private bool IsCameraInAllowedRangeOfPlanet(float distanceBetweenCameraAndPlanet)
        {
            return distanceBetweenCameraAndPlanet >= minPlanetDistance &&
                   distanceBetweenCameraAndPlanet <= maxPlanetDistance;
        }
    
        private void OnPinch(TKPinchRecognizer pinchInfo)
        {
            var positionChange = zoomSpeed * transform.forward * Time.unscaledDeltaTime;
            var futurePosition = transform.position + Mathf.Sign(pinchInfo.deltaScale) * positionChange;

            var distanceBetweenCameraAndPlanet = Vector3.Distance(futurePosition, planetTransform.position);
            if (IsCameraInAllowedRangeOfPlanet(distanceBetweenCameraAndPlanet))
            {
                transform.position = futurePosition;
            }
        }

        private void OnRotate(TKRotationRecognizer rotationInfo)
        {
            accumulatedRotation += rotationSpeedCoeffiecient * rotationInfo.deltaRotation;
        }

        private void OnPan(TKPanRecognizer panInfo)
        {
            cameraVelocity += swipeVelocityCoefficient * panInfo.deltaTranslation.magnitude;
            cameraDirection = 
                transform.TransformDirection(-panInfo.deltaTranslation.y, panInfo.deltaTranslation.x, 0);
        }

        private void Update()
        {
            cameraVelocity /= velocityDeclineCoefficient;
            if (cameraVelocity <= minStoppingVelocity)
            {
                cameraVelocity = 0f;
            }

            transform.RotateAround(planetTransform.position, cameraDirection, cameraVelocity * Time.unscaledDeltaTime);

            var deltaRotation = rotationSpeedCoeffiecient * accumulatedRotation * Time.unscaledDeltaTime;
            accumulatedRotation -= deltaRotation;

            var isTooSlow = Math.Abs(accumulatedRotation) <= deltaRotation;
            if (isTooSlow)
            {
                accumulatedRotation = 0;
            }
            else
            {
                transform.Rotate(0, 0, deltaRotation);
            }
        }
    }
}
