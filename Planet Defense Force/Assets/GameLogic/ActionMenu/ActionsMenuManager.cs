﻿namespace Assets.GameLogic.ActionMenu
{
    using System.Collections.Generic;
    using Actions;
    using Common;
    using Constants;
    using Data.Services;
    using Resources;
    using Upgrading;
    using UnityEngine;
    using UnityEngine.UI;
    using System;
    using Utils;
    using Planet;
    using InfoMessages;
    using UnityEngine.EventSystems;
    public class ActionsMenuManager : MonoBehaviour
    {
        [SerializeField]
        private SlideInOutMenu actionsGameMenu;

        [SerializeField]
        private Transform actionButton;

        [SerializeField]
        private RectTransform actionList;

        [SerializeField]
        private GatheredResources gatheredResources;
        
        private GameProgressData gameProgressData;
        private TowerUpgradeManager towerUpgradeManager;
        private PlanetBuildSlots buildSlots;
        
        private void Awake()
        {
            buildSlots = new PlanetBuildSlots();
            var tapListener = new TKTapRecognizer();
            tapListener.gestureRecognizedEvent += HideMenu;
            TouchKit.addGestureRecognizer(tapListener);
        }

        private void Start()
        {
            gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();
            towerUpgradeManager = GameObject.FindGameObjectWithTag(Tags.TowerUpgradeManager).GetComponent<TowerUpgradeManager>();
        }

        private void HideMenu(TKTapRecognizer recongnizer)
        {
            var tappingUIElement = EventSystem.current.IsPointerOverGameObject();
            Debug.Log("tapping ui: " + tappingUIElement);
            Debug.Log("is menu visible: " + actionsGameMenu.IsMenuVisible);//(!tappingUIElement && actionsGameMenu.IsMenuVisible));
            if (!tappingUIElement && actionsGameMenu.IsMenuVisible)
            {
                actionsGameMenu.Hide();
            }
        }

        public void ShowBuildMenu(RaycastHit buildPoint, Transform buildingParent)
        {
            foreach (Transform child in actionList)
            {
                Destroy(child.gameObject);
            }

            var actionsService = new ActionsService();

            var buildingData = actionsService.GetBuildingData();

            foreach(var building in buildingData.buildings)
            {
                var _captured = building;
                var defaultDataLevel = 1;

                if (gameProgressData.Current.ContainsId(building.unlockedSaveId))
                {
                    if(!gameProgressData.Current.IsUnlocked(building.unlockedSaveId))
                    {
                        continue;
                    }
                }

                var viewModel = ActionButtonViewModelFactory.CreateAction(building, defaultDataLevel, () =>
                {
                    var centeredPosition = GeneralUtils.GetHitTriangleCenter(buildPoint);

                    // Prevents the tower from 'sinking' into the ground.
                    var groundLevelAdjustedPos = GeneralUtils.ChangeMagnitude(centeredPosition, buildPoint.point.magnitude);

                    if (!buildSlots.IsFree(buildPoint.triangleIndex))
                    {
                        return;
                    }

                    var placedBuilding = Instantiate(
                        Resources.Load(DataPaths.BuildingPrefabsResourcesPath + _captured.prefabName),
                        groundLevelAdjustedPos,
                        Quaternion.FromToRotation(Vector3.up, buildPoint.normal)) as GameObject;

                    placedBuilding.transform.parent = buildingParent;

                    buildSlots.TakeSlot(buildPoint.triangleIndex, placedBuilding.gameObject);
                });

                AddButtonToActionList(actionList, viewModel);
            }
            
            actionsGameMenu.Show();
        }

        public void ShowActionsMenu(GameObject clickedTower)
        {
            if (clickedTower == null)
            {
                return;
            }

            foreach (Transform child in actionList)
            {
                Destroy(child.gameObject);
            }

            var upgradesMenuData = clickedTower.GetComponent<IUpgradeActionsMenuData>();
            if (upgradesMenuData != null)
            {
                var upgrades = upgradesMenuData.GetUpgradeModels(towerUpgradeManager);

                foreach (var upgradeModel in upgrades)
                {
                    var viewModel = ActionButtonViewModelFactory.CreateUpgradeAction(towerUpgradeManager, upgradeModel);
                    AddButtonToActionList(actionList, viewModel, towerUpgradeManager.GetLevel(upgradeModel.GetComponent()));
                }
            }

            var actionMenuData = clickedTower.GetComponent<IActionMenuData>();
            if (actionMenuData != null)
            {
                AddListToActions(actionList, actionMenuData.GetActionMenuData());
            }

            actionsGameMenu.Show();
        }

        private void AddListToActions(RectTransform actionList, IEnumerable<ActionButtonViewModel> items)
        {
            foreach (var upgradeModel in items)
            {
                if (upgradeModel != null)
                {
                    AddButtonToActionList(actionList, upgradeModel);
                }
            }
        }

        private void AddButtonToActionList(RectTransform actionList, ActionButtonViewModel actionModel, int level = 1)
        {
            var upgradeButtonPrefab = Instantiate(actionButton);

            Text levelText = upgradeButtonPrefab.FindChild("Level").GetComponent<Text>();
            levelText.text = level.ToString(); 

            var prefabContainer = upgradeButtonPrefab.FindChild("Container");

            var title = prefabContainer.FindChild("Title").GetComponent<Text>();
            title.text = actionModel.Title;

            var gold = actionModel.UpgradeCost.Gold;
            var titanium = actionModel.UpgradeCost.Titanium;
            var wood = actionModel.UpgradeCost.Wood;

            prefabContainer.Find("GoldText").GetComponent<Text>().text = "" + gold;
            prefabContainer.Find("TitaniumText").GetComponent<Text>().text = "" + titanium;
            prefabContainer.Find("WoodText").GetComponent<Text>().text = "" + wood;

            var buttonComponent = upgradeButtonPrefab.GetComponent<Button>();

            var _captured = actionModel;
            buttonComponent.onClick.AddListener(() =>
            {
                if (gatheredResources.HasFunds(gold, titanium, wood))
                {
                    gatheredResources.PurchaseIfPossible(gold, titanium, wood);
                    _captured.OnClick();

                    InfoMessageFactory.CreateToast("Purchased " + actionModel.Title, false);
                }
                
                if (OnActionMenuClosed != null)
                {
                    OnActionMenuClosed.Invoke();
                }

                actionsGameMenu.Hide();
            });

            buttonComponent.transform.SetParent(actionList, false);
        }

        public Action OnActionMenuClosed { get; set; }
    }
}