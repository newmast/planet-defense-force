﻿namespace Assets.GameLogic.ActionMenu
{
    using Common;

    public class SlideInOutMenu : AnimatedGameMenu
    {
        protected override void OnShow()
        {
            Animator.Play("slide-in");
        }

        protected override void OnHide()
        {
            Animator.Play("slide-out");
        }
    }
}
