﻿namespace Assets.GameLogic.InfoMessages
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    public static class InfoMessageFactory
    {
        public static Transform Create(string title, string content, Action onClosed)
        {
            var messageBox = UnityEngine.Object.Instantiate(Resources.Load("InfoMessage/MessageBox")) as GameObject;

            var messageTransform = messageBox.transform;

            var messageTitle = messageTransform.FindChild("Box").FindChild("Title");
            messageTitle.GetComponent<Text>().text = title;

            var messageContent = messageTransform.FindChild("Box").FindChild("Text");
            messageContent.GetComponent<Text>().text = content;

            var messageButton = messageTransform.FindChild("Button");
            messageButton.GetComponent<Button>().onClick.AddListener(onClosed.Invoke);
            messageButton.GetComponent<Button>().onClick.AddListener(() => UnityEngine.Object.Destroy(messageBox));

            messageBox.transform.SetParent(GameObject.Find("UI").transform, false);

            return messageBox.transform;
        }

        public static Transform CreateToast(string text, bool fast)
        {
            var message = text;

            var toast = UnityEngine.Object.Instantiate(Resources.Load("InfoMessage/Toast")) as GameObject;
            var textChild = toast.transform.Find("Button/Text");
            var messageText = textChild.GetComponent<Text>();

            messageText.text = message;

            if (fast)
            {
                toast.GetComponent<Animator>().speed = 1.5f;
            }

            toast.GetComponent<Animator>().Play("top-to-bot");
            toast.transform.SetParent(GameObject.Find("UI").gameObject.transform, false);

            UnityEngine.Object.Destroy(toast, 5);

            return toast.transform; 
        }
    }
}
