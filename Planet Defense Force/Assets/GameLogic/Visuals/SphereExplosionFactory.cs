﻿namespace Assets.GameLogic.Visuals
{
    using UnityEngine;

    public static class SphereExplosionFactory
    {
        private static Transform Create(Transform transform, string pathToMaterial)
        {
            var explosion = Object.Instantiate(
                    Resources.Load("SphereExplosions/Explosion"),
                    transform.position,
                    Quaternion.identity) as GameObject;

            var material = Resources.Load(pathToMaterial) as Material;

            var sphereObject = explosion.transform.FindChild("Sphere");

            sphereObject.GetComponent<MeshRenderer>().material = material;
            sphereObject.GetComponent<ExplosionSphereController>().followedTransform = transform;

            return explosion.transform;
        }

        public static Transform CreateHealing(Transform transform)
        {
            return Create(transform, "SphereExplosions/HealingExplosion");
        }
        
        public static Transform CreateCanon(Transform transform)
        {
            return Create(transform, "SphereExplosions/CanonExplosion");
        }
    }
}
