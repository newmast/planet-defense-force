﻿namespace Assets.GameLogic.Visuals
{
    using Constants;
    using UnityEngine;

    public class OrbitPlanet : MonoBehaviour
    {
        private Vector3 rotationDirection;
        private float speed;

        private Transform planetTransform;

        private void Awake()
        {
            rotationDirection = Random.onUnitSphere;
            speed = 8f;
        }

        private void Start()
        {
            planetTransform = GameObject.FindGameObjectWithTag(Tags.Planet).transform;
        }
	
        private void Update()
        {
            transform.LookAt(planetTransform);
            transform.Rotate(rotationDirection * speed * Time.deltaTime);
        }
    }
}
