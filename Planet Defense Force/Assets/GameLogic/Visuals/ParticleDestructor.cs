﻿namespace Assets.GameLogic.Visuals
{
    using Extensions;
    using UnityEngine;

    public class ParticleDestructor : MonoBehaviour
    {
        [SerializeField]
        private float destroyAfterSeconds;

        private void Awake()
        {
            this.Invoke(DestoryParticle, destroyAfterSeconds);
        }

        public void DestoryParticle()
        {
            Destroy(gameObject);
        }
    }
}