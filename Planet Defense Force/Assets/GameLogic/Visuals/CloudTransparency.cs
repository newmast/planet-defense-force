﻿namespace Assets.GameLogic.Visuals
{
    using UnityEngine;

    public class CloudTransparency : MonoBehaviour
    {
        [SerializeField]
        private Renderer cloudRenderer;

        private void Start()
        {
            var color = cloudRenderer.material.color;
            var newColor = new Color(color.r, color.g, color.b, 0.8f);

            cloudRenderer.material.color = newColor;
        }
    }
}
