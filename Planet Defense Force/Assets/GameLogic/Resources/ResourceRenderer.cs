﻿namespace Assets.GameLogic.Resources
{
    using System.Globalization;
    using UnityEngine;
    using UnityEngine.UI;

    public class ResourceRenderer : MonoBehaviour
    {
        [SerializeField]
        private GatheredResources resources;

        [SerializeField]
        private Text woodText;

        [SerializeField]
        private Text titaniumText;

        [SerializeField]
        private Text goldText;

        private void Start()
        {
        }

        private void Update()
        {
            woodText.text = resources.Wood.ToString("# ### ### ###");
            titaniumText.text = resources.Titanium.ToString("# ### ### ###");
            goldText.text = resources.Gold.ToString("# ### ### ###");
        }
    }
}
