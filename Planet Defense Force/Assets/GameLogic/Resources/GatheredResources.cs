﻿namespace Assets.GameLogic.Resources
{
    using UnityEngine;

    public class GatheredResources : MonoBehaviour
    {
        [SerializeField]
        private int wood;
        [SerializeField]
        private int titanium;
        [SerializeField]
        private int gold;

        private int score;

        private void Start() { }

        public int Score
        {
            get
            {
                return score;
            }
        }

        public int Wood
        {
            get
            {
                return wood;
            }
        }

        public int Titanium
        {
            get
            {
                return titanium;
            }
        }

        public int Gold
        {
            get
            {
                return gold;
            }
        }

        public void AddWood(int amount)
        {
            wood += amount;
        }
    
        public void AddTitanium(int amount)
        {
            titanium += amount;
        }

        public void AddGold(int amount)
        {
            gold += amount;
        }

        public void AddScore(int amount)
        {
            score += amount;
        }

        public bool HasFunds(int requiredGold, int requiredTitanium, int requiredWood)
        {
            return
                Gold >= requiredGold &&
                Titanium >= requiredTitanium &&
                Wood >= requiredWood;
        }

        public void PurchaseIfPossible(int goldCost, int titaniumCost, int woodCost)
        {
            if (HasFunds(goldCost, titaniumCost, woodCost))
            {
                AddGold(-goldCost);
                AddTitanium(-titaniumCost);
                AddWood(-woodCost);
            }
        }
    }
}
