﻿using UnityEngine;

namespace Assets.Common
{
    public class ListMenuManager : MonoBehaviour
    {
        public void ClearList(Transform listObject)
        {
            foreach (Transform child in listObject)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
