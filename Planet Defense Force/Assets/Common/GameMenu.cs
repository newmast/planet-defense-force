﻿using UnityEngine;

namespace Assets.Common
{
    public abstract class AnimatedGameMenu : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;

        protected Animator Animator
        {
            get { return this.animator; }
        }

        protected abstract void OnShow();

        protected abstract void OnHide();

        public void Show()
        {
            OnShow();
            IsMenuVisible = true;
        }

        public void Hide()
        {
            IsMenuVisible = false;
            OnHide();
        }

        public bool IsMenuVisible { get; private set; }
    }
}