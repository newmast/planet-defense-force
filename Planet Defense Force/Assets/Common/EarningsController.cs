﻿using Assets.Constants;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Common
{
    public class EarningsController : MonoBehaviour
    {
        [SerializeField]
        private Text coinsAmount;

        [SerializeField]
        private Text diamondsAmount;

        private GameProgressData gameProgressData;

        private void Start()
        {
            this.gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();
            Refresh();
        }

        public void Refresh()
        {
            this.coinsAmount.text = "" + this.gameProgressData.Current.playerCoins;
            this.diamondsAmount.text = "" + this.gameProgressData.Current.playerDiamonds;
        }
    }
}
