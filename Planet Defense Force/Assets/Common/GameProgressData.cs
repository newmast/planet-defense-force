﻿namespace Assets.Common
{
    using LevelMenu;
    using System;
    using Assets.Data.Models;
    using Assets.Data.Services;
    using Assets.Data.Services.PlayServices;
    using UnityEngine;

    public class GameProgressData : MonoBehaviour
    {
        private const string FirstTimeSaveDataKey = "FirstTimeSaveDataKey123455";
        private PlayGamesService playGamesService;
        private LocalStorageService localStorageService;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            playGamesService = new PlayGamesService();
            localStorageService = new LocalStorageService();

            playGamesService.Initialize();
        }

        public void Authenticate(Action<bool> callback)
        {
            playGamesService.Authenticate(callback);
        }

        private void Save(bool isOnline, Action callback)
        {
            if (isOnline && playGamesService.IsAuthenticated)
            {
                playGamesService.Save(Current, callback);
            }
            else
            {
                if (Current == null)
                {
                    return;
                }

                localStorageService.Save(Current);
                callback.Invoke();
            }
        }

        public void SeedDefaultSaveModel()
        {
            var saveData = new SaveDataService().GetUpgradesSaveData().saveData;
            var upgrades = new UpgradeProgress[saveData.Count];

            for (var i = 0; i < upgrades.Length; i++)
            {
                upgrades[i] = new UpgradeProgress
                {
                    upgradeId = saveData[i].saveId,
                    level = saveData[i].defaultValue
                };
            }

            Current = new SaveGameModel
            {
                playerCoins = 5000,
                playerDiamonds = 100,
                packsProgress = new PackProgress[LevelPopulator.NumberOfPacks],
                upgradesProgress = upgrades
            };

            Current.versionIdentification = Guid.NewGuid().ToString();
        }

        public void RestoreDataFromCloud(SaveGameModel cloudData)
        {
            Current = cloudData;
            localStorageService.Save(cloudData);
        }

        public void OverwriteCloudData(SaveGameModel localData, Action callback)
        {
            Current = localData;
            playGamesService.Save(localData, callback);
        }

        private bool IsStartingForTheFirstTime()
        {
            return PlayerPrefs.GetInt(FirstTimeSaveDataKey, 1) == 1;
        }

        private void LoadAndVerify(bool isOnline, Action callback, Action<SaveGameModel, SaveGameModel> onConflict)
        {
            Debug.Log("is online: " + isOnline);
            if (isOnline)
            {
                playGamesService.Load((cloudModel) =>
                {
                    Debug.Log("cloud model loaded");
                    if (!cloudModel.versionIdentification.Equals(
                        localStorageService.Load().versionIdentification))
                    {
                        Debug.Log("conflict");
                        onConflict.Invoke(cloudModel, localStorageService.Load());
                    }
                    else
                    {
                        Debug.Log("No conflict");
                        Current = localStorageService.Load();
                        callback.Invoke();
                    }
                });
            }
            else
            {
                Debug.Log("we arent online so callback.invoke");
                Current = localStorageService.Load();
                callback.Invoke();
            }
        }

        public void Sync(bool isOnline, Action callback, Action<SaveGameModel, SaveGameModel> onConflict)
        {
            Debug.Log("is online: " + isOnline);
            Debug.Log("is authenticated: " + playGamesService.IsAuthenticated);
            Debug.Log("is starting for the first time: " + IsStartingForTheFirstTime());
            if (localStorageService.Load() == null)
            {
                SeedDefaultSaveModel();

                Debug.Log("is local null: " + (localStorageService.Load() == null));
                
                Save(false, () => LoadAndVerify(false, callback, onConflict));
                
                if (playGamesService.IsAuthenticated)
                {
                    PlayerPrefs.SetInt(FirstTimeSaveDataKey, 2);
                    Save(true, () => LoadAndVerify(true, callback, onConflict));
                }
            }
            else
            {
                LoadAndVerify(isOnline && playGamesService.IsAuthenticated, callback, onConflict);
            }
        }

        public SaveGameModel Current { get; set; }

        public void UpdateProgress()
        {
            Save(false, () => Save(true, () => { }));
        }
    }
}