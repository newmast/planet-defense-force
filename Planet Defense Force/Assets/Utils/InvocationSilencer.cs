﻿namespace Assets.Utils
{
    using System;
    using UnityEngine;

    public class InvocationSilencer
    {
        private Action<float> silenceInvocation;

        private float silenceTime;
        private bool isSilenced;
        private int timerRefreshes;

        public InvocationSilencer(Action<float> silenceInvocation)
        {
            this.silenceInvocation = silenceInvocation;
        }

        public void DisableSilence()
        {
            timerRefreshes--;
            if (timerRefreshes == 0)
            {
                isSilenced = false;
            }
        }

        public void Silence(float seconds)
        {
            isSilenced = true;
            timerRefreshes++;
            silenceInvocation.Invoke(seconds);
        }

        public bool IsSilenced()
        {
            return isSilenced;
        }
    }
}
