﻿namespace Assets.Utils
{
    using Constants;
    using UnityEngine;

    public static class GameplayUtils
    {
        private const int MaxRaycastDistance = 50;

        public static bool SphereCast(out RaycastHit hitInfo, Vector3 touchLocation)
        {
            var clickPositionRay = Camera.main.ScreenPointToRay(touchLocation);
            clickPositionRay.origin = Camera.main.transform.position;
            return Physics.SphereCast(clickPositionRay, 0.01f, out hitInfo, MaxRaycastDistance, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Collide);
        }

        public static bool IsInLineOfSight(Vector3 from, Vector3 to, float range, string expectedTag)
        {
            var hit = new RaycastHit();
            if (Physics.Linecast(from, to, out hit, Physics.AllLayers))
            {
                if (hit.transform.gameObject.tag == expectedTag)
                {
                    return true;
                }
            }

            return false;
        }
        
        public static GameObject GetTappedObject(Vector2 screenTapLocation, out RaycastHit hitInfo)
        {
            if (SphereCast(out hitInfo, screenTapLocation))
            {
                if (hitInfo.collider != null)
                {
                    return GetGameObject(hitInfo);
                }
            }

            return null;
        }

        public static GameObject GetGameObject(RaycastHit hit)
        {
            return hit.collider.gameObject;
        }
        
        private static bool TestIfOnLayer(GameObject checkedObject, string layerName)
        {
            return checkedObject.layer == LayerMask.NameToLayer(layerName);
        }

        public static bool IsTower(GameObject checkedObject)
        {
            return checkedObject.tag == Tags.Tower;
        }

        public static bool IsLand(GameObject checkedObject)
        {
            return checkedObject.tag == Tags.PlanetLand;
        }
    }
}
