﻿namespace Assets.Utils
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public static class GeneralUtils
    {
        public static void RevertSceneOnBackPressed(string toScene)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(toScene);
            }
        }

        public static Vector3 GetHitTriangleCenter(RaycastHit hit)
        {
            MeshCollider meshCollider = hit.collider as MeshCollider;

            var mesh = meshCollider.sharedMesh;
            var normals = mesh.normals;
            var triangles = mesh.triangles;

            var point0 = normals[triangles[hit.triangleIndex * 3 + 0]];
            var point1 = normals[triangles[hit.triangleIndex * 3 + 1]];
            var point2 = normals[triangles[hit.triangleIndex * 3 + 2]];

            var baryCenter = hit.barycentricCoordinate;
            var interpolatedNormal = point0 * baryCenter.x + point1 * baryCenter.y + point2 * baryCenter.z;

            var hitTransform = hit.collider.transform;
            return hitTransform.TransformPoint(interpolatedNormal);
        }

        public static Vector3 ChangeMagnitude(Vector3 changedVector, float newMagnitude)
        {
            var adjustmentCoefficient = newMagnitude / changedVector.magnitude;
            var adjustmentVector = new Vector3(adjustmentCoefficient, adjustmentCoefficient, adjustmentCoefficient);

            changedVector.Scale(adjustmentVector);

            return changedVector;
        }
    }
}
