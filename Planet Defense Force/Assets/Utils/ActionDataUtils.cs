﻿namespace Assets.Utils
{
    using Data.Models;
    using Data.Models.Upgrades;
    using System.Linq;
    using UnityEngine;
    public static class ModelUtils
    {
        public static T GetLevelDetails<T>(AllDataValues<T> data, int level) where T : DataValue
        {
            return data
                .dataValues
                .FirstOrDefault(x => x.level == level);
        }

        public static ActionModel GetActionModelDetails(ActionItemData data, int level)
        {
            return data
                .details
                .FirstOrDefault(x => x.level == level);
        }
    }
}
