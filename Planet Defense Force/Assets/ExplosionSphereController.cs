﻿using UnityEngine;

public class ExplosionSphereController : MonoBehaviour
{
    private float scale = 0;

    private void Start()
    {
        transform.localScale = new Vector3(0, 0, 0);
        MaxScale = 1;
    }

    private void Update()
    {
        if (followedTransform == null)
        {
            return;
        }

        if (scale > MaxScale)
        {
            Destroy(transform.parent.gameObject);
            return;
        }

        scale += 2 * Time.deltaTime;

        transform.position = followedTransform.position;
        transform.localScale = new Vector3(scale, scale, scale);
    }

    public float MaxScale { get; set; }

    public Transform followedTransform;
}
