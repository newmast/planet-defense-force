﻿{
    "title": "Range",
    "backgroundUrl": "wtf.png",
    "details": [
      {
        "level": 1,
        "cost": {
          "gold": 15
        }
      },
      {
        "level": 2,
        "cost": {
          "gold": 30
        }
      },
      {
        "level": 3,
        "cost": {
          "gold": 50
        }
      },
      {
        "level": 4,
        "cost": {
          "gold": 70
        }
      },
      {
        "level": 5,
        "cost": {
          "gold": 100
        }
      },
      {
        "level": 6,
        "cost": {
          "gold": 100,
          "titanium": 10
        }
      },
      {
        "level": 7,
        "cost": {
          "gold": 150,
          "titanium": 20
        }
      },
      {
        "level": 8,
        "cost": {
          "gold": 200,
          "titanium": 40
        }
      },
      {
        "level": 9,
        "cost": {
          "gold": 250,
          "titanium": 60
        }
      },
      {
        "level": 10,
        "cost": {
          "gold": 300,
          "titanium": 80
        }
      }
    ]
  }