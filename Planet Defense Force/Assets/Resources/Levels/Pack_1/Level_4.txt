{
	"lanes": [
		{
			"enemyLaneStream": [
				{
					"groupId": 2,
					"spawnAfterMs": 5000
				},
				{
					"groupId": 3,
					"spawnAfterMs": 30000
				}
			]
		},
        {
			"enemyLaneStream": [
				{
					"groupId": 3,
					"spawnAfterMs": 30000
				}
			]
		}
	]
}