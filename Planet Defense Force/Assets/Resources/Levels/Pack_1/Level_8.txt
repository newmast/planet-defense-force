{
	"lanes": [
		{
			"enemyLaneStream": [
				{
					"groupId": 1,
					"spawnAfterMs": 5000
				},
				{
					"groupId": 2,
					"spawnAfterMs": 30000
				},
				{
					"groupId": 3,
					"spawnAfterMs": 30000
				}
			]
		},
        {
			"enemyLaneStream": [
				{
					"groupId": 5,
					"spawnAfterMs": 30000
				}
			]
		},
        {
			"enemyLaneStream": [
				{
					"groupId": 4,
					"spawnAfterMs": 15000
				}
			]
		}
	]
}