{
	"lanes": [
		{
			"enemyLaneStream": [
				{
					"groupId": 2,
					"spawnAfterMs": 5000
				},
				{
					"groupId": 2,
					"spawnAfterMs": 30000
				}
			]
		},
        {
			"enemyLaneStream": [
				{
					"groupId": 4,
					"spawnAfterMs": 30000
				},
				{
					"groupId": 5,
					"spawnAfterMs": 30000
				}
			]
		}
	]
}