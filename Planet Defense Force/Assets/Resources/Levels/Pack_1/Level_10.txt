{
	"lanes": [
		{
			"enemyLaneStream": [
				{
					"groupId": 2,
					"spawnAfterMs": 5000
				},
				{
					"groupId": 4,
					"spawnAfterMs": 30000
				}
			]
		},
        {
			"enemyLaneStream": [
				{
					"groupId": 5,
					"spawnAfterMs": 30000
				}
			]
		},
        {
			"enemyLaneStream": [
				{
					"groupId": 6,
					"spawnAfterMs": 45000
				}
			]
		}
	]
}