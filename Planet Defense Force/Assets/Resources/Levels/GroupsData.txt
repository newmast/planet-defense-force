{
	"allGroupsData": [
		{
			"id": "1",
			"name": "SingleEnemyGroup"
		},
		{
			"id": "2",
			"name": "WeakEnemyGroup"
		},
		{
			"id": "3",
			"name": "WeakEnemyGroup2"
		},
		{
			"id": "4",
			"name": "HealerGroup1"
		},
		{
			"id": "5",
			"name": "HealerGroup2"
		},
		{
			"id": "6",
			"name": "HealerGroup3"
		}
	]
}