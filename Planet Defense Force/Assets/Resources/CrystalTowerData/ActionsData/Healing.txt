﻿{
	"title": "Heal",
	"backgroundUrl": "heal.png",
	"details": [
		{
			"level": 1,
			"cost": {
				"wood": 0,
				"titanium": 0,
				"gold": 20
			}
		},
		{
			"level": 2,
			"cost": {
				"wood": 0,
				"titanium": 0,
				"gold": 35
			}
		},
		{
			"level": 3,
			"cost": {
				"wood": 0,
				"titanium": 0,
				"gold": 65
			}
		}
	]
}