﻿{
    "dataValues": [
      {
        "level": 1,
        "radius": 0.4,
		"aoeDamage": 0.5
      },
      {
        "level": 2,
        "radius": 0.6,
		"aoeDamage": 0.6
      },
      {
        "level": 3,
        "radius": 0.8,
		"aoeDamage": 0.7
      },
      {
        "level": 4,
        "radius": 0.95,
		"aoeDamage": 0.8
      },
      {
        "level": 5,
        "radius": 1.1,
		"aoeDamage": 0.9
      },
      {
        "level": 6,
        "radius": 1.3,
		"aoeDamage": 1
      },
      {
        "level": 7,
        "radius": 1.5,
		"aoeDamage": 1.1
      },
      {
        "level": 8,
        "radius": 1.7,
		"aoeDamage": 1.2
      },
      {
        "level": 9,
        "radius": 2,
		"aoeDamage": 1.3
      },
      {
        "level": 10,
        "radius": 2.5,
		"aoeDamage": 1.6
      }
    ]
}