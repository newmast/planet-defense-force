﻿{
    "dataValues": [
      {
        "level": 1,
        "value": 0.2
      },
      {
        "level": 2,
        "value": 0.4
      },
      {
        "level": 3,
        "value": 0.6
      },
      {
        "level": 4,
        "value": 0.8
      },
      {
        "level": 5,
        "value": 1
      },
      {
        "level": 6,
        "value": 1.2
      },
      {
        "level": 7,
        "value": 1.4
      },
      {
        "level": 8,
        "value": 1.6
      },
      {
        "level": 9,
        "value": 1.8
      },
      {
        "level": 10,
        "value": 2
      }
    ]
  }