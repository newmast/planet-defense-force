﻿{
	"dataValues": [
		{
			"level": 1,
			"allowedBounces": 1
		},
		{
			"level": 2,
			"allowedBounces": 2
		},
		{
			"level": 3,
			"allowedBounces": 3
		},
		{
			"level": 4,
			"allowedBounces": 4
		}
	]
}