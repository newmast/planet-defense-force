﻿{
	"welcome": {
		"title": "Welcome!",
		"content": "This seems to be your first time playing. Here's some clarification!"
	},
	"landTap": {
		"title": "Building",
		"content": "Tapping on land lets you build awesome towers!"
	},
	"tapTower": {
		"title": "Upgrading",
		"content": "Tapping on towers lets you upgrade them!"
	},
	"incomingAliens": {
		"title": "Defending",
		"content": "Defend your crystal from the incoming aliens by building towers!"
	}
}