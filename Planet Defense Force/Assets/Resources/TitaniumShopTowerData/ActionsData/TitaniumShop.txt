﻿{
	"title": "Titanium",
	"backgroundUrl": "titanium.png",
	"details": [
		{
			"level": 1,
			"cost": {
				"wood": 20,
				"titanium": 0,
				"gold": 0
			}
		},
		{
			"level": 2,
			"cost": {
				"wood": 80,
				"titanium": 0,
				"gold": 0
			}
		},
		{
			"level": 3,
			"cost": {
				"wood": 210,
				"titanium": 0,
				"gold": 0
			}
		},
		{
			"level": 4,
			"cost": {
				"wood": 500,
				"titanium": 0,
				"gold": 0
			}
		}
	]
}