﻿using UnityEngine;

namespace Assets
{
    using UnityEngine.UI;

    public class PauseButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        [SerializeField]
        private Sprite pausedImage;

        [SerializeField]
        private Sprite unpausedImage;

        private bool isPaused;
        
        private void Start()
        {
            button.onClick.AddListener(PauseOrUnpause);
            button.onClick.AddListener(ToggleState);
        }

        private void ToggleState()
        {
            if (isPaused)
            {
                button.image.sprite = pausedImage;
            }
            else
            {
               button.image.sprite = unpausedImage;
            }
        }

        private void PauseOrUnpause()
        {
            if (isPaused)
            {
                Unpause();
            }
            else
            {
                Pause();
            }
        }

        private void Pause()
        {
            Time.timeScale = 0f;
            isPaused = true;
        }

        private void Unpause()
        {
            Time.timeScale = 1;
            isPaused = false;
        }

        public bool IsPaused
        {
            get
            {
                return isPaused;
            }
        }
    }
}
