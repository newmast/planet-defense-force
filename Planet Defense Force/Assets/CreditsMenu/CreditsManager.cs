﻿namespace Assets.CreditsMenu
{
    using UnityEngine;
    using Data.Services;
    using Data.Models.Credits;
    using UnityEngine.UI;
    using Utils;
    public class CreditsManager : MonoBehaviour
    {
        [SerializeField]
        private RectTransform parentScrollView;

        [SerializeField]
        private Transform creditPanel;

        private CreditsList credits;

        private void Awake()
        {
            var creditsService = new CreditsService();
            credits = creditsService.GetCredits();
        }

        private void Start()
        {
            foreach (var credit in credits.credits)
            {
                var panel = Instantiate(creditPanel);

                panel.FindChild("Title")
                    .GetComponent<Text>()
                    .text = credit.title;

                panel.FindChild("Description")
                    .GetComponent<Text>()
                    .text = credit.description;

                panel.SetParent(parentScrollView);
            }
        }

        private void Update()
        {
            GeneralUtils.RevertSceneOnBackPressed("Scenes/MainMenu");
        }
    }
}
