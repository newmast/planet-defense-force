﻿namespace Assets.LevelMenu
{
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;
    using Utils;

    public class LevelPopulator : MonoBehaviour
    {
        public const int LevelsPerPack = 20;
        public const int NumberOfPacks = 1;

        [SerializeField]
        private Transform buttonPrefab;

        [SerializeField]
        private ChosenLevelData chosenLevelData;

        private GridLayoutGroup grid;
    
        private int packId = 1;

        private void Start()
        {
            grid = GetComponent<GridLayoutGroup>();
            GenerateGrid();
        }

        private void Update()
        {
            GeneralUtils.RevertSceneOnBackPressed("MainMenu");
        }

        public void GoToPrevious()
        {
            if (packId == 1)
            {
                return;
            }

            packId--;
            GenerateGrid();
        }

        public void GoToNext()
        {
            if (packId == NumberOfPacks)
            {
                return;
            }

            packId++;
            GenerateGrid();
        }

        private void GenerateGrid()
        {
            ClearGrid();
            for (var level = 1; level <= LevelsPerPack; level++)
            {
                CreateButton(level);
            }
        }

        private void ClearGrid()
        {
            for (var i = 0; i < grid.transform.childCount; i++)
            {
                Destroy(grid.transform.GetChild(i).gameObject);
            }
        }

        private void CreateButton(int level)
        {
            var buttonTransform = Instantiate(buttonPrefab);
            var buttonObject = buttonTransform.gameObject;
        
            var title = buttonTransform.FindChild("Text").GetComponent<Text>();
            title.text = level.ToString();

            var button = buttonObject.GetComponent<Button>();

            button.onClick.AddListener(() =>
            {
                chosenLevelData.PackId = packId;
                chosenLevelData.Level = level;
                chosenLevelData.MaxLevel = LevelsPerPack;
                SceneManager.LoadScene("Scenes/GameScene");
            });

            // Hardcoded behaviour: REDO
            if (level > 10)
            {
                button.interactable = false;

                var colors = button.colors;
                colors.disabledColor = Color.red;
                button.colors = colors;
            }

            buttonObject.transform.SetParent(grid.transform, false);
        }
    }
}
