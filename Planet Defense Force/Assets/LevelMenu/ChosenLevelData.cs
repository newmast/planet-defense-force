﻿namespace Assets.LevelMenu
{
    using UnityEngine;

    public class ChosenLevelData : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public int PackId { get; set; }

        public int Level { get; set; }

        public int MaxLevel { get; set; }
    }
}