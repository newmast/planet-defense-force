﻿namespace Assets.MainMenu
{
    using UnityEngine;
    using UnityEngine.UI;

    public class SoundManager : MonoBehaviour
    {
        [SerializeField]
        private Button clickedButton;

        private void Start()
        {
            clickedButton.onClick.AddListener(() => AudioListener.volume = 1 - AudioListener.volume);
        }
    }
}