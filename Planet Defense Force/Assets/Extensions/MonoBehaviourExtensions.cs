﻿namespace Assets.Extensions
{
    using System;
    using UnityEngine;

    public static class MonoBehaviourExtensions
    {
        public static void InvokeRepeating(this MonoBehaviour m, Action method, float waitTime, float repeatRate)
        {
            m.InvokeRepeating(method.Method.Name, waitTime, repeatRate);
        }

        public static void Invoke(this MonoBehaviour m, Action method, float waitTime)
        {
            m.Invoke(method.Method.Name, waitTime);
        }
    }
}
