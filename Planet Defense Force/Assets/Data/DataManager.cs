﻿namespace Assets.Data
{
    using UnityEngine;

    public class DataManager
    {
        private AssetManager assetManager;
        
        public DataManager(AssetManager assetManager)
        {
            this.assetManager = assetManager;
        }
        
        public T Get<T>(string relativeFilePath)
        {
            var json = assetManager.ReadFile(relativeFilePath);
            return JsonUtility.FromJson<T>(json);
        }
    }
}