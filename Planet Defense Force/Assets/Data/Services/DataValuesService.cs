﻿namespace Assets.Data.Services
{
    using Constants;
    using Models.Enemies;
    using Models.OtherActions;
    using Models.Upgrades;

    public class DataValuesService : DataService
    {
        private AllDataValues<T> GetDataValue<T>(string towerDataPath, string dataValueName) where T : DataValue
        {
            return Get<AllDataValues<T>>(towerDataPath + "DataValues/" + dataValueName);
        }

        public AllDataValues<DamageModel> GetDamageData(string towerDataPath)
        {
            return GetDataValue<DamageModel>(towerDataPath, "Damage");
        }

        public AllDataValues<AttackSpeedModel> GetAttackSpeedData(string towerDataPath)
        {
            return GetDataValue<AttackSpeedModel>(towerDataPath, "AttackSpeed");
        }
        
        public AllDataValues<RangeModel> GetRangeData(string towerDataPath)
        {
            return GetDataValue<RangeModel>(towerDataPath, "Range");
        }

        public AllDataValues<WoodModel> GetWoodData()
        {
            return GetDataValue<WoodModel>(DataPaths.WoodData, "Wood");
        }

        public AllDataValues<HealingModel> GetCrystalHealingData()
        {
            return GetDataValue<HealingModel>(DataPaths.CrystalData, "Healing");
        }

        public AllDataValues<TitaniumShopModel> GetTitaniumShopDataModel()
        {
            return GetDataValue<TitaniumShopModel>(DataPaths.TitaniumShopData, "TitaniumShop");
        }

        public AllDataValues<ExplosionRadiusModel> GetExplosionRadiusData(string towerDataPath)
        {
            return GetDataValue<ExplosionRadiusModel>(towerDataPath, "ExplosionRadius");
        }

        public AllDataValues<SilenceModel> GetSilenceData(string towerDataPath)
        {
            return GetDataValue<SilenceModel>(towerDataPath, "Silence");
        }

        public AllDataValues<BounceCountModel> GetBounceCountData(string towerDataPath)
        {
            return GetDataValue<BounceCountModel>(towerDataPath, "BounceCount");
        }

        public T GetEnemyDataModel<T>(string dataPath)
        {
            return Get<T>(dataPath);
        }
    }
}
