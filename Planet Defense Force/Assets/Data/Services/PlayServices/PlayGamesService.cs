﻿namespace Assets.Data.Services.PlayServices
{
    using System;
    using Models;
    using UnityEngine;

    using GooglePlayGames;
    using GooglePlayGames.BasicApi;
    using GooglePlayGames.BasicApi.SavedGame;

    public class PlayGamesService
    {
        private const string SaveGameName = "PlanetDefenseForceSave8";

        private bool saving;
        private bool loading;

        private Action onSavedAction;
        private Action<SaveGameModel> onLoadedAction;
        private SaveGameModel savedModel;

        public void Initialize()
        {
            var config = new PlayGamesClientConfiguration.Builder()
                .EnableSavedGames()
                .Build();

            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = false;

            PlayGamesPlatform.Activate();
        }

        public bool IsAuthenticated
        {
            get
            {
                return PlayGamesPlatform.Instance.IsAuthenticated();
            }
        }

        public void Authenticate(Action<bool> callback)
        {
            PlayGamesPlatform.Instance.Authenticate(callback);
        }

        public void Load(Action<SaveGameModel> callback)
        {
            onLoadedAction = callback;
            saving = false;
            loading = true;
            OpenSavedGame();
        }

        public void Save(SaveGameModel model, Action callback)
        {
            onSavedAction = callback;
            savedModel = model;
            saving = true;
            loading = false;
            OpenSavedGame();
        }

        private void OpenSavedGame()
        {
            var savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(SaveGameName, DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened);
        }

        private void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                if (loading)
                {
                    LoadGameData(game);
                }

                if (saving)
                {
                    SaveGameData(game, savedModel.ToBytes());
                }
            }
            else
            {
                // handle error
            }
        }

        // READ
        private void LoadGameData(ISavedGameMetadata game)
        {
            var savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
        }

        private void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                onLoadedAction.Invoke(SaveGameModel.FromBytes(data));
            }
            else
            {
                // TODO: Handle error.
            }
        }
        
        private void SaveGameData(ISavedGameMetadata game, byte[] savedData)
        {
            var savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            var updatedMetadata = new SavedGameMetadataUpdate
                .Builder()
                .WithUpdatedDescription("Saved game at " + DateTime.Now)
                .Build();

            savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
        }

        private void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                onSavedAction.Invoke();
            }
            else
            {
                // TODO: Handle error.
            }
        }
    }
}
