﻿namespace Assets.Data.Services
{
    using Constants;
    using Models.Store;

    public class StoreService : DataService
    {
        public TowerUpgradesPurchaseList GetTowerUpgradeModels()
        {
            return Get<TowerUpgradesPurchaseList>(DataPaths.StoreShopData + "TowerUpgrades");
        }

        public PowerUpsPurchaseList GetPowerUpsModels()
        {
            return Get<PowerUpsPurchaseList>(DataPaths.StoreShopData + "PowerUps");
        }

        public DiamondPacksStoreModel GetDiamondPacks()
        {
            return Get<DiamondPacksStoreModel>(DataPaths.StoreShopData + "DiamondPacks");
        }
    }
}
