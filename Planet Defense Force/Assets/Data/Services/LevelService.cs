﻿namespace Assets.Data.Services
{
    using Constants;
    using Models.Levels;
    using System.Collections.Generic;
    using Models.Enemies;

    public class LevelService : DataService
    {
        private readonly LaneData[] lanes;
        private readonly Dictionary<int, string> groups;

        public LevelService(int packNumber, int level)
        {
            var path = string.Format(DataPaths.LevelDataFormat, packNumber, level);

            groups = new Dictionary<int, string>();
            
            Get<AllGroupsData>(DataPaths.AllGroupsData)
                .allGroupsData
                .ForEach(x => groups.Add(x.id, x.name));

            lanes = Get<LevelData>(path)
                .lanes;
        }

        public int GetNumberOfLanes()
        {
            return lanes.Length;
        }

        public int GetSpawnTimeInMs(int laneIndex, int groupIndex)
        {
            if (AnyLeft(laneIndex, groupIndex))
            {
                return lanes[laneIndex]
                    .enemyLaneStream[groupIndex]
                    .spawnAfterMs;
            }

            return -1;
        }

        public int GetNumberOfEnemies(int laneIndex)
        {
            return lanes[laneIndex].enemyLaneStream.Length;
        }

        public string GetGroupTag(int laneIndex, int groupIndex)
        {
            if (AnyLeft(laneIndex, groupIndex))
            {
                var enemyGroupId = lanes[laneIndex]
                    .enemyLaneStream[groupIndex]
                    .groupId;

                return groups[enemyGroupId];
            }

            return null;
        }

        public bool AnyLeft(int laneIndex, int groupIndex)
        {
            return groupIndex + 1 <= lanes[laneIndex].enemyLaneStream.Length;
        }
    }
}
