﻿namespace Assets.Data.Services
{
    using Constants;
    using Models;

    public class SaveDataService : DataService
    {
        public SaveData GetUpgradesSaveData()
        {
            return Get<SaveData>(DataPaths.SaveData);
        }
    }
}
