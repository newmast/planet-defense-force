﻿namespace Assets.Data.Services.Store
{
    using System;
    using UnityEngine;
    using UnityEngine.Purchasing;

    public class InAppPurchaseManager : IStoreListener
    {
        private string smallestDiamondPackId;
        private string smallDiamondPackId;
        private string mediumDiamondPackId;
        private string bigDiamondPackId;

        private IStoreController storeController;
        
        public InAppPurchaseManager(
            string smallestPackId, 
            string smallPackId, 
            string mediumPackId, 
            string bigPackId)
        {
            smallestDiamondPackId = smallestPackId;
            smallDiamondPackId = smallPackId;
            mediumDiamondPackId = mediumPackId;
            bigDiamondPackId = bigPackId;

            var module = StandardPurchasingModule.Instance();
            var builder = ConfigurationBuilder.Instance(module);

            builder.AddProduct(smallestDiamondPackId, ProductType.Consumable);
            builder.AddProduct(smallDiamondPackId, ProductType.Consumable);
            builder.AddProduct(mediumDiamondPackId, ProductType.Consumable);
            builder.AddProduct(bigDiamondPackId, ProductType.Consumable);

            UnityPurchasing.Initialize(this, builder);
        }

        public IStoreController GetStoreController()
        {
            return storeController;
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            storeController = controller;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("OnInitializeFailed: " + error);
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            var message = string.Format(
                "OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",
                product.definition.storeSpecificId,
                failureReason);

            Debug.Log(message);
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            var productId = e.purchasedProduct.definition.id;
            if (productId.Equals(smallestDiamondPackId))
            {
                if (OnSmallestBought != null)
                {
                    OnSmallestBought.Invoke();
                }
            }
            else if (productId.Equals(smallDiamondPackId))
            {
                if (OnSmallBought != null)
                {
                    OnSmallBought.Invoke();
                }
            }
            else if (productId.Equals(mediumDiamondPackId))
            {
                if (OnMediumBought != null)
                {
                    OnMediumBought.Invoke();
                }
            }
            else if (productId.Equals(bigDiamondPackId))
            {
                if (OnBigBought != null)
                {
                    OnBigBought.Invoke();
                }
            }

            return PurchaseProcessingResult.Complete;
        }

        public Action OnSmallestBought { get; set; }

        public Action OnSmallBought { get; set; }

        public Action OnMediumBought { get; set; }

        public Action OnBigBought { get; set; }
    }
}
