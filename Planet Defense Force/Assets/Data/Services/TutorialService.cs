﻿namespace Assets.Data.Services
{
    using Models.Tutorial;

    public class TutorialService : DataService
    {
        public AllTutorials GetAllTutorials()
        {
            return Get<AllTutorials>("Tutorial/Tutorial");
        }
    }
}
