﻿namespace Assets.Data.Services
{
    public abstract class DataService
    {
        private readonly DataManager dataManager;

        protected DataService()
        {
            dataManager = new DataManager(new AssetManager());
        }

        protected T Get<T>(string dataPath)
        {
            return dataManager
                .Get<T>(dataPath);
        }
    }
}
