﻿namespace Assets.Data.Services
{
    using Models;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;

    public class LocalStorageService
    {
        private const string SaveFileName = "SaveGame4.txt";

        public SaveGameModel Load()
        {
            var path = Path.Combine(Application.persistentDataPath, SaveFileName);

            SaveGameModel model;

            if (!File.Exists(path))
            {
                return null;
            }

            BinaryFormatter formatter = new BinaryFormatter();
            using (var fileStream = new FileStream(path, FileMode.Open))
            {
                model = (SaveGameModel)formatter.Deserialize(fileStream);
            }

            return model;
        }

        public void Save(SaveGameModel model)
        {
            var path = Path.Combine(Application.persistentDataPath, SaveFileName);

            BinaryFormatter formatter = new BinaryFormatter();
            using (var fileStream = File.Create(path))
            {
                formatter.Serialize(fileStream, model);
            }
        }
    }
}
