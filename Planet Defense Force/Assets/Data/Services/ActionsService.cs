﻿namespace Assets.Data.Services
{
    using Models;
    using Constants;

    public class ActionsService : DataService
    {
        private ActionItemData GetActionItemData(string towerDataPath, string actionName)
        {
            return Get<ActionItemData>(towerDataPath + "ActionsData/" + actionName);
        }

        public ActionItemData GetDamageActionModel(string towerDataPath)
        {
            return GetActionItemData(towerDataPath, "Damage");
        }

        public ActionItemData GetExplosionRadiusActionModel(string towerDataPath)
        {
            return GetActionItemData(towerDataPath, "ExplosionRadius");
        }
        public ActionItemData GetSilenceActionModel(string towerDataPath)
        {
            return GetActionItemData(towerDataPath, "Silence");
        }
        public ActionItemData GetBounceCountActionModel(string towerDataPath)
        {
            return GetActionItemData(towerDataPath, "BounceCount");
        }

        public ActionItemData GetAttackSpeedActionModel(string towerDataPath)
        {
            return GetActionItemData(towerDataPath, "AttackSpeed");
        }

        public ActionItemData GetRangeActionModel(string towerDataPath)
        {
            return GetActionItemData(towerDataPath, "Range");
        }

        public ActionItemData GetWoodActionModel()
        {
            return GetActionItemData(DataPaths.WoodData, "Wood");
        }

        public ActionItemData GetCrystalHealingActionModel()
        {
            return GetActionItemData(DataPaths.CrystalData, "Healing");
        }

        public ActionItemData GetTitaniumShopActionModel()
        {
            return GetActionItemData(DataPaths.TitaniumShopData, "TitaniumShop");
        }

        public AllBuildingsData GetBuildingData()
        {
            return Get<AllBuildingsData>(DataPaths.BuildingData + "ActionsData/BuildingData");
        }
    }
}
