﻿using Assets.Constants;
using Assets.Data.Models.Credits;

namespace Assets.Data.Services
{
    public class CreditsService : DataService
    {
        public CreditsList GetCredits()
        {
            return Get<CreditsList>(DataPaths.CreditsData);
        }
    }
}
