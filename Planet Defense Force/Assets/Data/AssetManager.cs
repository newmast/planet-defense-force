﻿namespace Assets.Data
{
    using UnityEngine;

    public class AssetManager
    {
        public string ReadFile(string relativePathToFile)
        {
            TextAsset asset = Resources.Load(relativePathToFile) as TextAsset;
            if (asset == null)
                Debug.Log(relativePathToFile);
            return asset.text;
        }
    }
}
