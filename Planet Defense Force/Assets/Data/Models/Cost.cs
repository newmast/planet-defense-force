﻿namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class Cost
    {
        public int wood;

        public int titanium;

        public int gold;
    }
}
