﻿namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class SingleSaveData
    {
        public string saveId;
        public int defaultValue;
    }
}
