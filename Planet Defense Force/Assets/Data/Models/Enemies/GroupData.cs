﻿namespace Assets.Data.Models.Enemies
{
    using System;

    [Serializable]
    public class GroupData
    {
        public int id;

        public string name;
    }
}