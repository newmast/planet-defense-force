﻿using Assets.Data.Services;
using System;
using System.Collections.Generic;

namespace Assets.Data.Models.Enemies
{
    [Serializable]
    public class AllGroupsData
    {
        public List<GroupData> allGroupsData;
    }
}
