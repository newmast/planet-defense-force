﻿namespace Assets.Data.Models.Enemies
{
    using System;

    [Serializable]
    public class BaseEnemyDataModel
    {
        public float maxHealth;
        public float range;
        public float attackSpeed;

        public int goldWorth;
        public int scoreWorth;
    }
}
