﻿namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class ActionItemData
    {
        public string title;

        public string backgroundUrl;

        public ActionModel[] details;
    }
}
