﻿namespace Assets.Data.Models.OtherActions
{
    using System;
    using Upgrades;

    [Serializable]
    public class HealingModel : DataValue
    {
        public float health;
    }
}
