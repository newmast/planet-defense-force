﻿namespace Assets.Data.Models.OtherActions
{
    using System;
    using Upgrades;

    [Serializable]
    public class TitaniumShopModel : DataValue
    {
        public int titanium;
    }
}
