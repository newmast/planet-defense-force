﻿
namespace Assets.Data.Models.Levels
{
    using System;

    [Serializable]
    public class LevelData
    {
        public LaneData[] lanes;
    }
}
