﻿namespace Assets.Data.Models.Levels
{
    using System;

    [Serializable]
    public class LaneData
    {
        public EnemyLevelData[] enemyLaneStream;
    }
}