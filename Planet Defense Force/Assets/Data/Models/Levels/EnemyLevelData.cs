﻿using System;

namespace Assets.Data.Models.Levels
{
    [Serializable]
    public class EnemyLevelData
    {
        public int groupId;
        public int spawnAfterMs;
    }
}