﻿namespace Assets.Data.Models
{
    using System;
    
    [Serializable]
    public class AllBuildingsData
    {
        public SingleBuildingData[] buildings;
    }
}
