﻿namespace Assets.Data.Models.Tutorial
{
    using System;

    [Serializable]
    public class AllTutorials
    {
        public Tutorial welcome;

        public Tutorial landTap;

        public Tutorial tapTower;

        public Tutorial incomingAliens;
    }
}