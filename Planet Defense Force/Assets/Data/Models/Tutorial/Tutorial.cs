﻿namespace Assets.Data.Models.Tutorial
{
    using System;

    [Serializable]
    public class Tutorial
    {
        public string title;

        public string content;
    }
}
