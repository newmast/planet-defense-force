﻿namespace Assets.Data.Models.Store
{
    using System;

    [Serializable]
    public class DiamondPack
    {
        public string id;
        public string title;
        public int diamondsReward;
    }
}
