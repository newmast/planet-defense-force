﻿namespace Assets.Data.Models.Store
{
    using System;

    [Serializable]
    public class DiamondPacksStoreModel
    {
        public DiamondPack smallestPack;
        public DiamondPack smallPack;
        public DiamondPack mediumPack;
        public DiamondPack bigPack;
    }
}
