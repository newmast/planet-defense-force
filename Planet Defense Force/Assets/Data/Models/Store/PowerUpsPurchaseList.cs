﻿namespace Assets.Data.Models.Store
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class PowerUpsPurchaseList
    {
        public List<PurchaseModel> unlockablePowerUps;
    }
}
