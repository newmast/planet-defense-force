﻿namespace Assets.Data.Models.Store
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public class PurchaseModel
    {
        public string saveId;
        public List<PurchaseLevelData> purchaseLevelsData;
    }
}
 