﻿namespace Assets.Data.Models.Store
{
    using System;

    [Serializable]
    public class PurchaseLevelData
    {
        public int level;
        public string imagePath;
        public string title;
        public int coinsCost;
        public int diamondsCost;
    }
}
