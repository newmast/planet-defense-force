﻿namespace Assets.Data.Models.Store
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class TowerUpgradesPurchaseList
    {
        public List<PurchaseModel> unlockableTowerUpgrades;
    }
}
