﻿namespace Assets.Data.Models.Upgrades
{
    using System;

    [Serializable]
    public class AttackSpeedModel : DataValue
    {
        public float value;
    }
}
