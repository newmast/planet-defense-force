﻿namespace Assets.Data.Models.Upgrades
{
    using System;

    [Serializable]
    public class RangeModel : DataValue
    {
        public float value;
    }
}
