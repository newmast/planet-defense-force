﻿namespace Assets.Data.Models.Upgrades
{
    using System;

    [Serializable]
    public class SilenceModel : DataValue
    {
        public float silenceDuration;
    }
}
