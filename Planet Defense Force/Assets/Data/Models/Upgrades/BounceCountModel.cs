﻿using System;

namespace Assets.Data.Models.Upgrades
{
    [Serializable]
    public class BounceCountModel : DataValue
    {
        public int allowedBounces;
    }
}
