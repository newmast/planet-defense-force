﻿namespace Assets.Data.Models.Upgrades
{
    using System;

    [Serializable]
    public class ExplosionRadiusModel : DataValue
    {
        public float radius;

        public float aoeDamage;
    }
}
