namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class PackProgress
    {
        public int packId;
        public int reachedLevel;
    }
}