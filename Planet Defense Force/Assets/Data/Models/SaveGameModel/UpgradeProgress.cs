namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class UpgradeProgress
    {
        public string upgradeId;
        public int level;
    }
}