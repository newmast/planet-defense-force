﻿namespace Assets.Data.Models
{
    using System;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [Serializable]
    public class SaveGameModel
    {
        public PackProgress[] packsProgress;
        public UpgradeProgress[] upgradesProgress;

        public int playerCoins;
        public int playerDiamonds;

        public string versionIdentification;

        public bool ContainsId(string id)
        {
            return upgradesProgress.FirstOrDefault(x => x.upgradeId == id) != null;
        }

        public int GetUpgradeLevel(string id)
        {
            return upgradesProgress.FirstOrDefault(x => x.upgradeId == id).level;
        }

        public void SetUpgradeLevel(string id, int value)
        {
            upgradesProgress.FirstOrDefault(x => x.upgradeId == id).level = value;
        }

        public bool IsUnlocked(string id)
        {
            return upgradesProgress.FirstOrDefault(x => x.upgradeId == id).level >= 1;
        }
        
        public byte[] ToBytes()
        {
            return Encoding.Default.GetBytes(JsonUtility.ToJson(this));
        }

        public static SaveGameModel FromBytes(byte[] data)
        {
            if (data.Length == 0)
            {
                return null;
            }

            string json = Encoding.Default.GetString(data);
            return JsonUtility.FromJson<SaveGameModel>(json);
        }
    }
}
