﻿
namespace Assets.Data.Models.Credits
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class CreditsList
    {
        public List<CreditsModel> credits;
    }
}
