﻿namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class CreditsModel
    {
        public string title;

        public string description;        
    }
}
