﻿namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class ActionModel
    {
        public int level;

        public Cost cost;
    }
}