﻿namespace Assets.Data.Models.Upgrades
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class AllDataValues<T> where T : DataValue
    {
        public List<T> dataValues;
    }
}
