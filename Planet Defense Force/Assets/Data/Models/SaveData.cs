﻿namespace Assets.Data.Models
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SaveData
    {
        public List<SingleSaveData> saveData;
    }
}
