﻿namespace Assets.Data.Models
{
    using System;

    [Serializable]
    public class SingleBuildingData : ActionItemData
    {
        public string prefabName;
        public string unlockedSaveId;
        public string maxLevelSaveId;
    }
}