﻿namespace Assets.SplashScreenMenu
{
    using Common;
    using Constants;
    using System;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class SplashScreenController : MonoBehaviour
    {
        [SerializeField]
        private Button restoreOldDataButton;

        [SerializeField]
        private Button overwriteOldDataButton;
    
        [SerializeField]
        private Image background;

        private GameProgressData gameData;

        private void Start()
        {
            restoreOldDataButton.gameObject.SetActive(false);
            overwriteOldDataButton.gameObject.SetActive(false);
            gameData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();

#if UNITY_EDITOR
            gameData.SeedDefaultSaveModel();
            gameData.Current.SetUpgradeLevel(SaveIds.RandomCoinMeteorId, 1);
            gameData.Current.playerCoins = 500;
            gameData.Current.playerDiamonds = 1000;
            SceneManager.LoadScene("MainMenu");
#else
            Debug.Log("here");
            //gameData.Authenticate(hasSucceded=>
            //{
                Sync(false);
            //});
#endif
        }

        private void Sync(bool hasSucceded)
        {
            gameData.Sync(
                hasSucceded,
                () =>
                {
                    SceneManager.LoadScene("MainMenu");
                },
                (cloudData, localData) =>
                {
                    background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
                    restoreOldDataButton.gameObject.SetActive(true);
                    overwriteOldDataButton.gameObject.SetActive(true);

                    restoreOldDataButton.onClick.AddListener(() =>
                    {
                        gameData.RestoreDataFromCloud(cloudData);
                        SceneManager.LoadScene("MainMenu");
                    });

                    overwriteOldDataButton.onClick.AddListener(() =>
                    {
                        gameData.OverwriteCloudData(localData, () => SceneManager.LoadScene("MainMenu"));
                    });
                });
        }
    }
}
