﻿namespace Assets.ShopMenu
{
    using Data.Models.Store;
    using Data.Services;
    using UnityEngine;

    public class TowerUpgradesController : ShopListController
    {
        private TowerUpgradesPurchaseList model;

        private void Awake()
        {
            var storeService = new StoreService();
            model = storeService.GetTowerUpgradeModels();
        }

        protected override void Start()
        {
            base.Start();
            AddToContent(model.unlockableTowerUpgrades);
        }

        public override Transform GetButtonTitle(Transform prefabButton)
        {
            return prefabButton
                .Find("PurchaseName");
        }

        public override Transform GetCoinsButton(Transform prefabButton)
        {
            return prefabButton
                .Find("BuyCoins");
        }

        public override Transform GetDiamondsButton(Transform prefabButton)
        {
            return prefabButton
                .Find("BuyDiamonds");
        }

        public override void AddListToUserInterface()
        {
            AddToContent(model.unlockableTowerUpgrades);
        }
    }
}