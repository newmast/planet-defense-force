﻿namespace Assets.ShopMenu
{
    using System;
    using Common;
    using Constants;
    using Data.Models.Store;
    using Data.Services;
    using Data.Services.Store;
    using UnityEngine;
    using UnityEngine.UI;

    public class DiamondsPackPurchaser : MonoBehaviour
    {
        [SerializeField]
        private RectTransform content;

        [SerializeField]
        private Transform menuItemPrefab;

        private DiamondPacksStoreModel model;
        private InAppPurchaseManager iapManager;
        private GameProgressData gameProgressData;

        private void AddToContent(string title, Action purchaseCallback)
        {
            var menuItem = Instantiate(menuItemPrefab);

            menuItem
                .FindChild("Title")
                .GetComponent<Text>()
                .text = title;

            menuItem
                .FindChild("PurchaseButton")
                .FindChild("Text")
                .GetComponent<Text>()
                .text = "Purchase";

            menuItem
                .FindChild("PurchaseButton")
                .GetComponent<Button>()
                .onClick
                .AddListener(() => purchaseCallback.Invoke());

            menuItem.SetParent(content);
        }

        private void Awake()
        {
            var storeService = new StoreService();
            model = storeService.GetDiamondPacks();
            iapManager = new InAppPurchaseManager(
                model.smallestPack.id,
                model.smallPack.id,
                model.mediumPack.id,
                model.bigPack.id);
        }

        private void CreateContentViewModel(string id, string title, Action onIapItemBought, int diamondsReward)
        {
            AddToContent(title, () =>
            {
                onIapItemBought += () =>
                {
                    gameProgressData.Current.playerDiamonds += diamondsReward;
                };

                iapManager.GetStoreController().InitiatePurchase(id);
            });
        }

        public void AddList()
        {
            gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();

            CreateContentViewModel(model.smallestPack.id, model.smallestPack.title, iapManager.OnSmallestBought, model.smallestPack.diamondsReward);
            CreateContentViewModel(model.smallPack.id, model.smallPack.title, iapManager.OnSmallBought, model.smallPack.diamondsReward);
            CreateContentViewModel(model.mediumPack.id, model.mediumPack.title, iapManager.OnMediumBought, model.mediumPack.diamondsReward);
            CreateContentViewModel(model.bigPack.id, model.bigPack.title, iapManager.OnBigBought, model.bigPack.diamondsReward);
        }
    }
}