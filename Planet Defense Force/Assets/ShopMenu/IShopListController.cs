﻿namespace Assets.ShopMenu
{
    using UnityEngine;

    public interface IShopListController
    {
        Transform GetButtonTitle(Transform prefabButton);

        Transform GetCoinsButton(Transform prefabButton);

        Transform GetDiamondsButton(Transform prefabButton);

        void AddListToUserInterface();
    }
}