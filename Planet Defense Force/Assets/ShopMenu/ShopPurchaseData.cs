﻿namespace Assets.ShopMenu
{
    using UnityEngine;

    public class ShopPurchaseData : MonoBehaviour
    {
        public int CoinsCost { get; set; }

        public int DiamondsCost { get; set; }

        public bool IsBuyingWithCoins { get; set; }

        public string ItemId { get; set; }
    }
}
