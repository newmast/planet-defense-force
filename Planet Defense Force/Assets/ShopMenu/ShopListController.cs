﻿namespace Assets.ShopMenu
{
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using ShopMenu;
    using UnityEngine;
    using UnityEngine.UI;
    using Constants;
    using Data.Models.Store;
    using System;

    public abstract class ShopListController : MonoBehaviour, IShopListController
    {
        [SerializeField]
        private Transform content;

        [SerializeField]
        private Transform menuItemPrefab;

        [SerializeField]
        private ShopTransactionManager shopTransactonManager;

        [SerializeField]
        private EarningsController earningsController;

        private GameProgressData gameProgressData;

        private void AddButtonListeners(GameObject prefab, PurchaseModel data)
        {
            var prefabTransform = prefab.transform;
            var purchaseData = prefab.GetComponent<ShopPurchaseData>();

            Action purchaseAction = () =>
            {
                var purchaseWasSuccessful = shopTransactonManager.Purchase(purchaseData);
                if (purchaseWasSuccessful)
                {
                    RefreshButtonContent(prefab, data);
                    earningsController.Refresh();
                }
            };

            GetCoinsButton(prefabTransform)
                .GetComponent<Button>()
                .onClick
                .AddListener(() =>
                {
                    purchaseData.IsBuyingWithCoins = true;
                    purchaseAction.Invoke();
                });

            GetDiamondsButton(prefabTransform)
                .GetComponent<Button>()
                .onClick
                .AddListener(() =>
                {
                    purchaseData.IsBuyingWithCoins = false;
                    purchaseAction.Invoke();
                });
        }

        protected virtual void Start()
        {
            gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();
        }

        protected GameProgressData GetGameProgressData()
        {
            return gameProgressData;
        }

        protected ShopTransactionManager GetShopTransactionManager()
        {
            return shopTransactonManager;
        }

        private void SetButtonText(Transform button, string text)
        {
            button
                .FindChild("Text")
                .GetComponent<Text>()
                .text = text;
        }

        private void SetButtonInteractable(Transform button, bool state)
        {
            button
                .gameObject
                .GetComponent<Button>()
                .interactable = state;
        }

        private void RefreshButtonContent(GameObject prefab, PurchaseModel data)
        {
            var purchaseData = prefab.GetComponent<ShopPurchaseData>();

            if (purchaseData == null)
            {
                purchaseData = prefab.AddComponent<ShopPurchaseData>();
            }

            var itemLevel = gameProgressData.Current.GetUpgradeLevel(data.saveId);
            var itemData = data.purchaseLevelsData.FirstOrDefault(x => x.level == itemLevel + 1);
            var isMaxedOut = itemData == null;

            if (isMaxedOut)
            {
                itemData = data.purchaseLevelsData.FirstOrDefault(x => x.level == itemLevel);
            }

            purchaseData.CoinsCost = itemData.coinsCost;
            purchaseData.DiamondsCost = itemData.diamondsCost;
            purchaseData.ItemId = data.saveId;

            var prefabTransform = prefab.transform;

            GetButtonTitle(prefabTransform)
                .GetComponent<Text>()
                .text = itemData.title;

            SetButtonText(GetCoinsButton(prefabTransform), "" + itemData.coinsCost);
            SetButtonText(GetDiamondsButton(prefabTransform), "" + itemData.diamondsCost);

            if (isMaxedOut)
            {
                SetButtonInteractable(GetCoinsButton(prefabTransform), false);
                SetButtonInteractable(GetDiamondsButton(prefabTransform), false);

                SetButtonText(GetCoinsButton(prefabTransform), "MAX");
                SetButtonText(GetDiamondsButton(prefabTransform), "MAX");
            }
        }

        public void AddToContent(List<PurchaseModel> listItems)
        {
            foreach (var listItem in listItems)
            {
                var menuItem = Instantiate(menuItemPrefab);

                RefreshButtonContent(menuItem.gameObject, listItem);
                AddButtonListeners(menuItem.gameObject, listItem);

                menuItem.SetParent(content);
            }
        }

        public abstract Transform GetButtonTitle(Transform prefabButton);

        public abstract Transform GetCoinsButton(Transform prefabButton);

        public abstract Transform GetDiamondsButton(Transform prefabButton);

        public abstract void AddListToUserInterface();
    }
}
