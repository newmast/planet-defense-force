﻿namespace Assets.ShopMenu
{
    using Common;
    using Constants;
    using UnityEngine;
    using Utils;

    public class ShopTransactionManager : MonoBehaviour
    {
        private GameProgressData gameProgressData;

        private bool HasFunds(ShopPurchaseData purchaseData)
        {
            var availableCoins = gameProgressData.Current.playerCoins;
            var availableDiamonds = gameProgressData.Current.playerDiamonds;

            var requiredCoins = purchaseData.CoinsCost;
            var requiredDiamonds = purchaseData.DiamondsCost;
        
            if (purchaseData.IsBuyingWithCoins)
            {
                return availableCoins >= requiredCoins;
            }
            else
            {
                return availableDiamonds >= requiredDiamonds;
            }
        }

        public bool Purchase(ShopPurchaseData purchaseData)
        {
            if (!HasFunds(purchaseData))
            {
                return false;
            }

            if (purchaseData.IsBuyingWithCoins)
            {
                gameProgressData.Current.playerCoins -= purchaseData.CoinsCost;
            }
            else
            {
                gameProgressData.Current.playerDiamonds -= purchaseData.DiamondsCost;
            }

            var currentLevel = gameProgressData.Current.GetUpgradeLevel(purchaseData.ItemId);
            gameProgressData.Current.SetUpgradeLevel(purchaseData.ItemId, currentLevel + 1);
            gameProgressData.UpdateProgress();

            return true;
        }

        private void Start()
        {
            gameProgressData = GameObject.Find(FixedGameObjects.GameProgressData).GetComponent<GameProgressData>();
        }
        
        private void Update()
        {
            GeneralUtils.RevertSceneOnBackPressed("MainMenu");
        }
    }
}
