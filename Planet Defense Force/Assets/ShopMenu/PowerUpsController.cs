﻿namespace Assets.ShopMenu
{
    using Data.Models.Store;
    using Data.Services;
    using UnityEngine;

    public class PowerUpsController : ShopListController
    {
        private PowerUpsPurchaseList model;

        private void Awake()
        {
            var storeService = new StoreService();
            model = storeService.GetPowerUpsModels();
        }

        public override Transform GetButtonTitle(Transform prefabButton)
        {
            return prefabButton
                .Find("PurchaseName");
        }

        public override Transform GetCoinsButton(Transform prefabButton)
        {
            return prefabButton
                .Find("BuyCoins");
        }

        public override Transform GetDiamondsButton(Transform prefabButton)
        {
            return prefabButton
                .Find("BuyDiamonds");
        }

        public override void AddListToUserInterface()
        {
            AddToContent(model.unlockablePowerUps);
        }
    }
}
