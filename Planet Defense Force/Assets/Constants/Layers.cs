﻿namespace Assets.Constants
{
    public class Layers
    {
        public const string Default = "Default";
        public const string Tappable = "Tappable";
        public const string IgnoreFriendlyCollision = "Ignore Friendly Collision";
    }
}
