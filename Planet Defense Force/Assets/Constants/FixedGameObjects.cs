﻿namespace Assets.Constants
{
    public class FixedGameObjects
    {
        public const string ChosenLevelData = "/ChosenLevelData";
        public const string GameProgressData = "/GameProgressData";
    }
}
