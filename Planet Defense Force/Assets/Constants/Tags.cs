﻿namespace Assets.Constants
{
    public class Tags
    {
        public const string Enemy = "Enemy";
        public const string Planet = "Planet";
        public const string PlanetLand = "Land";
        public const string TowerUpgradeManager = "TowerUpgradeManager";
        public const string GatheredResources = "GatheredResources";
        public const string Tower = "Tower";
        public const string Crystal = "Crystal";
        public const string EnemyGroup = "EnemyGroup";
        public const string Water = "Water";
        public const string CoinMeteorManager = "CoinMeteorManager";
        public const string ActionsMenuManager = "ActionsMenuManager";
    }
}
