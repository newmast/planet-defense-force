﻿namespace Assets.Constants
{
    public class DataPaths
    {
        public const string LevelsPackFormat = "Levels/Pack_{0}/";

        public const string EnemyGroupsPrefabs = "EnemyGroupsPrefabs/";

        public const string BuildingData = "BuildingData/";

        public const string BuildingPrefabsResourcesPath = "BuildingPrefabs/";

        public const string WoodData = "WoodTowerUpgradeData/";
        public const string WeakTowerData = "WeakTowerUpgradeData/";
        public const string CannonTowerData = "CannonTowerUpgradeData/";
        public const string EmpTowerData = "EmpTowerUpgradeData/";
        public const string BouncerTowerData = "BouncerTowerUpgradeData/";

        public const string CrystalData = "CrystalTowerData/";
        public const string TitaniumShopData = "TitaniumShopTowerData/";
        public const string StoreShopData = "StoreShopData/";

        public const string EnemyData = "EnemyData/";

        public const string WeakEnemy = EnemyData + "WeakEnemyData";
        public const string TankEnemy = EnemyData + "TankEnemyData";
        public const string HealerEnemy = EnemyData + "HealerEnemyData";

        public const string AllGroupsData = "Levels/GroupsData";
        public const string LevelDataFormat = LevelsPackFormat + "Level_{1}";
        public const string SaveData = "SaveData/SaveData";

        public const string CreditsData = "Credits/Credits";
    }
}
